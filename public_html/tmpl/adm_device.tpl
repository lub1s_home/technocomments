<b>%name%</b><br />

%adm_device_edit%

<br /><b>Последнее изменение</b><br />
<form action='../functions.php' method='post'>
	<table>
		<tr>
			<td class='gray'>
				%lastmod%
				<input type='hidden' name='id' value='%device_id%' />
			</td>
			<td>
				<input type='text' name='confirm' class='short' placeholder='confirm update' required title='update / обновить' />
			</td>
			<td>
				<input type='submit' name='adm_update_device_lastmod' value='update' />
			</td>
		</tr>
	</table>
</form>

<br /><b>Дата выхода</b><br />
<form action='../functions.php' method='post'>
	<table>
		<tr>
			<td>
				<input type='hidden' name='id' value='%device_id%' />
				
				<input type='date' name='date' value='%release_date%' />
			</td>
			<td>
				<input type='text' name='confirm' class='short' placeholder='confirm edit' required title='edit / редактировать' />
			</td>
			<td>
				<input type='submit' name='adm_edit_device_release_date' value='edit' />
			</td>
		</tr>
	</table>
</form>

<br /><b>Характеристики</b><br />

<table>
	<tr>
		<td><i>id</i></td>
		<td><i>Name</i></td>
		<td><i>Value</i></td>
		<td colspan='3'><i>Action</i></td>
	</tr>

	%characteristics%

</table>

%select_pattern_char%

<br /><b>Добавить характеристику</b><br />

<form action='../functions.php' method='post'>
	<table>
		<tr>
			<td>
				<input class='long' type='text' name='name' placeholder='name' />
			</td>
			<td>
				<input class='long' type='text' name='value' placeholder='value' />
			</td>
			<td>
				<input type='hidden' name='device_id' value='%device_id%' />
				<input type='submit' name='adm_add_characteristic' value='add' />
			</td>
		</tr>
	</table>
</form>


<br /><b id='edit_or_delete_video'>Видеообзоры</b><br />

%message_edit_or_delete_video_err%
%message_edit_or_delete_video_suc%

%all_video%


<br /><b id='add_video'>Добавить видеообзор</b><br />

%message_add_video%

<form action='../functions.php' method='post'>
	<table>
		<tr>
			<td>
				<input class='long' type='text' name='source' placeholder='source' />
			</td>
			<td>
				<input type='hidden' name='device_id' value='%device_id%' />
				<input type='submit' name='adm_add_video' value='add' />
			</td>
		</tr>
	</table>
</form>


<br /><b id='edit_photo'>Редактировать фото</b><br />

<table class='device_photo'>

	%edit_photo%

</table>


<br /><b id='ozon_id'>Партнерка Ozon</b><br />

<form action='../functions.php' method='post'>
	<table>
		<tr>
			<td>
				<input value='%al_ozon%' class='long' type='text' name='al_ozon' placeholder='ozon id' />
			</td>
			<td>
				<input type='hidden' name='device_id' value='%device_id%' />
				<input type='submit' name='adm_edit_al_ozon' value='edit' />
			</td>
		</tr>
	</table>
</form>


<br /><b id='affiliate_link'>Партнерка Aliexpress</b><br />

<form action='../functions.php' method='post'>
	<table>
		<tr>
			<td>
				<input value='%al_aliexpress%' class='long' type='text' name='al_aliexpress' placeholder='affiliate link' />
			</td>
			<td>
				<input type='hidden' name='device_id' value='%device_id%' />
				<input type='submit' name='adm_edit_p_aliexpress' value='edit' />
			</td>
		</tr>
	</table>
</form>


<br /><b>Специальные символы</b><br />
<p>×</p>