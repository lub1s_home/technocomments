<!DOCTYPE html>
<html lang='ru'>
<head>
	<meta charset="UTF-8" />
	<title>%title%</title>
	<meta name='keywords' content='%meta_keys%' />
	<meta name='description' content='%meta_desc%' />
	<meta name='author' content='Алексей Курза' />
	<link rel='stylesheet' href='%address%/main.css' />
	<link rel='canonical' href='%address%%canonical%' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0' />
	<!--[if lt IE 9]>
		<script>
			document.createElement('article');
			document.createElement('dl');
			document.createElement('dt');
			document.createElement('dd');
			document.createElement('header');
			document.createElement('main');
			document.createElement('nav');
			document.createElement('section');
			document.createElement('time');
			document.createElement('footer');
		</script>
	<![endif]-->

    
    <!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter36798010 = new Ya.Metrika({ id:36798010, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/36798010" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
	
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-58649613-2', 'auto');
	  ga('send', 'pageview');

	</script>
</head>

<body>

<div id='blackout'></div>

<header class='header_site' style='position: relative; z-index: 10;'>

	<div class='wr'>
		<div class='standard_block'>
			<a class='logo' href='/'>
				<span style='font-size: 80px;'>t</span>echno<br /><span style='margin-left: 7px; font-size: 17px;'>comments</span>
				<!--<img src='/images/logo.jpg' alt='TechnoComments логотип' />-->
			</a>
			<div class='site_description'>
				Отзывы о компьютерной, мобильной технике и комплектующих
			</div>
			<div id='switch_buttons'>
				<div style='background-image: url("/images/menu_devices.png"); margin-left: 11px;' class='slide_but'>
					<div></div>
				</div>
				
				%menu_brands_button%
				
			</div>
		</div>
	</div>
	
	<div class='wr'>
		<div id='device_types'>
			<nav>
			
				%menu_sections_items%
				
			</nav>
			
			%menu_brands%
			
		</div>
	</div>
	
	<script>
		'use strict';
		var bool_sw = [false, false];
		var sw = document.getElementById('switch_buttons');
		var dt = document.getElementById('device_types');
		var b_out = document.getElementById('blackout');
		
		sw.onclick = function(event)
		{
			var event = event || window.event;
			var target = event.target || event.srcElement;
			
			for ( var i = 0; i < sw.children.length; i++ )
			{
				if ( target == sw.children[i] || target == sw.children[i].children[0] )
				{
					for ( var j = 0; j < sw.children.length; j++ )
					{
						if ( bool_sw[j] )
						{
							sw.children[j].children[0].style.left = '-11px';
							sw.children[j].style.backgroundPosition = '-110px';
							/*dt.children[j].style.zIndex = '-1';
							dt.children[j].style.opacity = '0';*/
							dt.children[j].style.display = 'none';
							bool_sw[j] = false;
						}
						else
							if ( j == i )
							{
								sw.children[j].children[0].style.left = '98px';
								sw.children[j].style.backgroundPosition = '0px';
								/*dt.children[j].style.zIndex = '10';
								dt.children[j].style.opacity = '1';*/
								dt.children[j].style.display = 'block';
								b_out.className = 'blackout';
								bool_sw[j] = true;
							}
					}
				}
			}
			for ( i = 0; i < sw.children.length; i++ )
			{
				if (bool_sw[i] == true)
					break;
				if ((i + 1) == sw.children.length)
					b_out.className = '';
			}
		}
	</script>
</header>

<main>

	%middle%
	
</main>

<div class='pre_footer'></div>
<footer class='footer_site'>
	<div class='wr'>
		<div class='standard_block_footer'>
			Сотрудничество: <span>forwebmasterak@gmail.com</span><br /><br />
			Релиз Technocomments: 14 апреля 2016
		
			<div class='wr' style='overflow: hidden; margin-top: 20px;'>
				<div class='counter_tc'>
					<!-- этот div выводит тИЦ -->
					<a href="http://yandex.ru/cy?base=0&amp;host=http://technocomments.info/"><img src="http://www.yandex.ru/cycounter?http://technocomments.info/" width="88" height="31" alt="Индекс цитирования" /></a>
				</div>
				<div class='counter_tc' id='top100_widget'><!-- этот div выводит Rambler Top100 --></div>
				<div class='counter_tc'>
					<!--этот div выводит  LiveInternet counter--><script type="text/javascript"><!--
					document.write("<a href='//www.liveinternet.ru/click' "+
					"target=_blank><img src='//counter.yadro.ru/hit?t52.1;r"+
					escape(document.referrer)+((typeof(screen)=="undefined")?"":
					";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
					screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
					";"+Math.random()+
					"' alt='' title='LiveInternet: показано число просмотров и"+
					" посетителей за 24 часа' "+
					"border='0' width='88' height='31'><\/a>")
					//--></script><!--/LiveInternet-->
				</div>
			</div>
		</div>
	</div>	
</footer>

<!-- Rambler Top100 (Kraken) Widget -->
<script>
    (function (w, d, c) {
    (w[c] = w[c] || []).push(function() {
        var options = {
            project: 4451035,
            element: 'top100_widget'
        };
        try {
            w.top100Counter = new top100(options);
        } catch(e) { }
    });
    var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src =
    (d.location.protocol == "https:" ? "https:" : "http:") +
    "//st.top100.ru/top100/top100.js";

    if (w.opera == "[object Opera]") {
    d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(window, document, "_top100q");
</script>
<noscript><img src="//counter.rambler.ru/top100.cnt?pid=4451035"></noscript>
<!-- END Top100 (Kraken) Counter -->

</body>
</html>