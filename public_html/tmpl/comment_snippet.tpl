<article itemprop='review' itemscope itemtype='http://schema.org/Review'>
	<footer>
		<strong itemprop='author'>%name%</strong><br />%for_admin_edit%
		<time itemprop='datePublished' content='%schema_ORG_datePublished%' datetime='%datetime%'>%date%</time>
	</footer>
	<p itemprop='reviewBody'>%comment%</p>
</article>