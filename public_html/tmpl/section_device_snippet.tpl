<a class='device_list_item' href='%address%/%link%'>
	<div class='circle_rating'>
		<div style='background-position: 0 %CSS_position%px; height: %CSS_height%px;'></div>
		<div>%first_digit%.<span>%last_digit%</span></div>
	</div>
	<figure>
		<img src='%image%' alt='%name_type% %name_company% %name_device%' />
		<figcaption>
			<div>%name_device%</div>
			<div>%name_company%</div>
			<div>%name_type%</div>
		</figcaption>
	</figure>
</a>