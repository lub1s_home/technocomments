<div id='reviews' class='wr'>
	<div class='standard_block'>
		<div>
			<h2>%name_singular% - оставить отзыв</h2>
		</div>
		<div class='brad_crumb'>
			<a href='/'>Главная</a> →
			<a href='/%section_link%'>%name_plural%</a> →
			Оставить отзыв
		</div>
		<div>
			<div class='add_review'>
				
				%message%
			
				<select id='select_device_type'>
					
					%options%
					
				</select>
				
				<script>
					'use strict';
					var sel = document.getElementById('select_device_type');
					sel.onchange = function()
					{
						var device = document.forms[0].elements.device.value;
						var name = document.forms[0].elements.name.value;
						var comment = document.forms[0].elements.comment.value;
						var link = sel.options[sel.selectedIndex].value + '/offer#reviews';
						
						document.write('<form action="/functions.php" method="post"> <input type="text" name="device" value="' + device + '" /> <input type="text" name="name" value="' + name + '" /> <textarea name="comment">' + comment + '</textarea> <input type="text" name="link" value="' + link + '" />  <input type="text" name="offer_save_data_and_redirect" value="1" /> </form>');
						
						document.forms[0].submit();
					}
				</script>
				
				<form action='/functions.php' method='post'>
				
					<input name='device' type='text' value='%device%' placeholder='Название' required />
					<input name='name' type='text' value='%name%' placeholder='Имя' required />
					<textarea maxlength='3000' placeholder='Отзыв' rows='7' name='comment' required>%comment%</textarea>
					
					<input type='hidden' name='count_questions' value='%count_questions%' />
					<input type='hidden' name='device_name_singular' value='%name_singular%' />
					
					%questions%
					
					<input type='submit' name='offer' value='Опубликовать'>
				</form>
			</div>
		</div>
	</div>
</div>