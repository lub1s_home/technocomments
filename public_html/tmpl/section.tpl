<div class='wr'>
	<header class='standard_block'>
		<div>
			<h1>%type_device%%brand%%page%</h1>
		</div>
		<div class='brad_crumb'>
			%bread_crumb%
		</div>
		
		%offer_or_description%

	</header>
</div>

<div class='wr'>

	%section_device_snippets%

	<div style='clear: both'></div>
</div>

<div class='wr'>
	<div class='standard_block pagination'>
	
		%pagination%
	
	</div>
</div>