<div itemscope itemtype='http://schema.org/Product'>
	<div class='wr'>
		<header class='standard_block'>
			<div>
				<h1 itemprop='name'>%name%</h1>
			</div>
			<div class='brad_crumb'>
				<a href='/'>Главная</a> →
				<a href='/%bread_crumb_sec_link%'>%bread_crumb_sec_name%</a> →
				<a href='/%bread_crumb_sec_link%/%bread_crumb_com_link%'>%bread_crumb_com_name%</a> →
				%bread_crumb_dev_name%
			</div>
			<div class='bookmark mrg_b'>
				%for_admin_edit%
				<a href='#buy'>Где купить</a> 
				<a href='#photo'>Фото</a> 
				<a href='#character'>Технические характеристики</a> 
				<a href='#video'>Видео-обзоры</a> 
				<a href='#assessment'>Оценка</a> 
				<a href='#reviews'>Отзывы</a> 
				<a href='#add_review'>Оставить отзыв</a>
			</div>
		</header>
	</div>

	<section class='wr'>
		<div id='buy' class='standard_block'>
			<div>
				<h2>Где купить</h2>
			</div>
			<div>
			
				%affiliate_links%
				
			</div>
		<div>
	</section>

	<section class='wr'>
		<div id='photo' class='standard_block'>
			<div>
				<h2>Фото</h2>
			</div>
			<div class='brad_crumb'>
				Клик по фото - смотреть следующее фото
			</div>
			<div class='photo'>
				
				%photo%
				
				<div id='switch_photo'>
					
				</div>
				
			</div>
			<script>
				'use strict';
				var photo = document.getElementsByClassName('photo')[0];
				var index = 0;
				var click_sw;
				
				var sw_photo = document.getElementById('switch_photo');
				
				for (var i = 0; i < photo.children.length - 1; i++)
				{
					sw_photo.appendChild( document.createElement('div') );
				}
				
				photo.onclick = function(event)
				{
					click_sw = false;
					
					// если клик по переключателям
					for (var i = 0; i < photo.children.length - 1; i++)
					{
						if (event.target == sw_photo.children[ i ])
						{
							click_sw = true;
							
							photo.children[ index ].style.opacity = '0';
							photo.children[ i ].style.opacity = '1';
							
							sw_photo.children[ index ].style.backgroundColor = 'transparent';
							sw_photo.children[ i ].style.backgroundColor = '#b50040';
							
							index = i;
							return;
						}
					}
					
					// если клик по фото
					if (!click_sw)
					{
						photo.children[ index ].style.opacity = '0';
						sw_photo.children[ index ].style.backgroundColor = 'transparent';
						
						index++;
						if (index == photo.children.length - 1) 
							index = 0;
							
						photo.children[ index ].style.opacity = '1';
						sw_photo.children[ index ].style.backgroundColor = '#b50040';
					}
				}
			</script>
		</div>
	</section>

	<section class='wr'>
		<div id='character' class='standard_block'>
			<div>
				<h2>Технические характеристики</h2>
			</div>
			
			%official_site%
			
			<div class='mrg_b'>
				<table class='character' itemprop='description'>
				
					%device_characteristics%
				
				</table>
			</div>
			
			<!--<div style='margin: 20px 0 10px 0'>
				Здесь была реклама до изменения ее стиля.
			</div>-->
			
		</div>
	</section>

	<section class='wr'>
		<div id='video' class='standard_block'>
			<div>
				<h2>Видео-обзоры</h2>
			</div>
			<div class='video mrg_b'>
			
				%video%
			
			</div>
		</div>
	</section>

	<section class='wr'>
		<div id='assessment' class='standard_block'>
			<div>
				<h2>Оценка</h2>
			</div>
			<div itemprop='aggregateRating' itemscope itemtype='http://schema.org/AggregateRating'>
			
				%rating%
				
			</div>
		</div>
	</section>
	
	%device_ad%

	<section class='wr'>
		<div id='reviews' class='standard_block'>
			<div>
				<h2>Отзывы</h2>
			</div>
			
			<div class='reviews mrg_b'>
				<div class='brad_crumb'>
					Мнения реальных владельцев %bread_crumb_com_name% %bread_crumb_dev_name%
				</div>
				
				%comments_show%
				
			</div>
			
			<!--<div style='margin: 20px 0 10px 0'>
				Здесь была реклама до изменения ее стиля.
			</div>-->
			
			
			<div class='brad_crumb' style='font-style: normal; margin-bottom: 0;'>
				<h6><strong>Понравилось на сайте?</strong> - Сделайте его лучше.</h6>
				Напишите про устройства, которыми пользуетесь, выпущенными в этом или прошлом году.
				Найдите их в меню или напишите <a onclick="yaCounter36798010.reachGoal('addReviewOther'); return true;" href='/%bread_crumb_sec_link%/offer'>здесь</a>.<br />
				Или рекомендуйте друзьям.<br />
				
				<!-- Соц кнопки -->
				<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
				<script src="//yastatic.net/share2/share.js"></script>
				<!-- Цель в метрике -->
				<div style='margin: 10px 0 0; display: inline-block;' onclick="yaCounter36798010.reachGoal('socButtons'); return true;">
					<div style='margin-bottom: 10px' class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus,twitter,viber,whatsapp,skype,telegram"></div>
				</div>
				
				
			</div>
		</div>
	</section>
</div>

<div class='wr' style='margin-bottom: 20px; max-width: 730px;'>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- TC -->
	<ins class="adsbygoogle"
		 style="display:block"
		 data-ad-client="ca-pub-4994716008321849"
		 data-ad-slot="1122582017"
		 data-ad-format="auto"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
</div>

<section class='wr'>
	<div id='add_review' class='standard_block'>
		<div>
			<h2>Оставить отзыв</h2>
		</div>
		<div>
		
			%message%
			
		</div>
		<div class='add_review'>
			<form name='add_comment' action='/functions.php' method='post' id='add_comment'>
				<input name='name' type='text' value='%name_for_comment%' placeholder='Имя' required />
				<textarea maxlength='3000' placeholder='Несколько слов о девайсе...' rows='7' name='comment' required>%comment_for_comment%</textarea>
				<input type='hidden' name='device_id' value='%device_id%' />
				<input type='hidden' name='count_questions' value='%count_questions%' />
				
				%questions%
				
				<input type='submit' onclick="yaCounter36798010.reachGoal('addReviewCurrent'); return true;" name='add_comment' value='Опубликовать'>
			</form>
		</div>
	</div>
</section>