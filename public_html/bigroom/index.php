<?php
// это админка!

mb_internal_encoding('UTF-8');
require_once '../lib/database_class.php';
require_once '../lib/adm_commentscontent_class.php';
require_once '../lib/adm_devicecontent_class.php';
require_once '../lib/adm_frontpagecontent_class.php';
require_once '../lib/adm_companycontent_class.php';

$db = DataBase::getObject();
$view = (isset($_GET['view'])) ?
	$_GET['view'] :
	'';
switch ($view) 
{
	case '':
		$content = new Adm_FrontPageContent($db);
		break;
	case 'comments':
		$content = new Adm_CommentsContent($db);
		break;
	case 'device':
		$content = new Adm_DeviceContent($db);
		break;
	case 'company':
		$content = new Adm_CompanyContent($db);
		break;
}
echo $content->getContent();
?>