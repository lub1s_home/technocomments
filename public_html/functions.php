<?
require_once 'lib/database_class.php';
require_once 'lib/manage_class.php';

$db = DataBase::getObject();
$manage = new Manage($db);


if ($_POST['add_comment']) 
	$r = $manage->addComment();

elseif ($_POST['offer']) 
	$r = $manage->offer();
	
elseif ($_POST['offer_save_data_and_redirect']) 
	$r = $manage->offerSaveDataAndRedirect();
	


// админка:
elseif ($_POST['adm_auth']) 
	$r = $manage->adminAuth();

elseif ($_GET['adm_logout']) 
	$r = $manage->adminLogout();

	
	
elseif ($_POST['adm_add_company']) 
	$r = $manage->adminAddCompany();
	
elseif (isset($_POST['submit_edit_company'])) 
	$r = $manage->adminEditCompany();

elseif (isset($_POST['submit_delete_company'])) 
	$r = $manage->adminDeleteCompany();



elseif ($_POST['adm_add_characteristic']) 
	$r = $manage->adminAddCharacteristic();

elseif (isset($_POST['submit_edit_char'])) 
	$r = $manage->adminEditCharacteristics();

elseif ($_POST['adm_delete_characteristic']) 
	$r = $manage->adminDeleteCharacteristic();
	
elseif ($_POST['adm_select_pattern_char_for_device']) 
	$r = $manage->adminSelectAddPatternCharForDevice();



elseif ($_POST['adm_add_device']) 
	$r = $manage->adminAddDevice();

elseif ($_POST['adm_edit_device']) 
	$r = $manage->adminEditDevice();
	
elseif ($_POST['adm_update_device_lastmod']) 
	$r = $manage->adminUpdateDeviceLastmod();

elseif ($_POST['adm_edit_device_release_date']) 
	$r = $manage->adminEditDeviceReleaseDate();
	
elseif ($_POST['adm_edit_p_aliexpress']) 
	$r = $manage->adminEditAffiliateLinkAliexpress();
	
elseif ($_POST['adm_edit_al_ozon']) 
	$r = $manage->adminEditOzonId();



elseif ($_POST['adm_comment_approve']) 
	$r = $manage->adminApproveComment();

elseif ($_POST['adm_comment_delete']) 
	$r = $manage->adminDeleteComment();



elseif ($_POST['adm_edit_photo']) 
	$r = $manage->adminEditPhoto();

elseif ($_POST['adm_delete_photo']) 
	$r = $manage->adminDeletePhoto();



elseif ($_POST['adm_add_video']) 
	$r = $manage->adminAddVideo();

elseif ($_POST['adm_edit_video']) 
	$r = $manage->adminEditVideo();

elseif ($_POST['adm_delete_video']) 
	$r = $manage->adminDeleteVideo();



else exit;
$manage->redirect($r);
?>