<?php

require_once 'global_class.php';
	
class Company extends GlobalClass 
{
	public function __construct($db) 
	{
		parent::__construct('company', $db);
	}
		
	public function setTitle($old_value, $new_value) 
	{
		return $this->setField('title', $new_value, 'title', $old_value);
	}
	
	public function editCompany($id, $title, $site, $wordstat)
	{
		$upd_fields = array('title' => $title, 'site' => $site, 'wordstat' => $wordstat, );
		$this->updateOnID($id, $upd_fields);
	}
	
	public function addCompany($title, $site, $wordstat)
	{
		return $this->add(array('title' => $title, 'site' => $site, 'wordstat' => $wordstat));
	}
	
	public function getName($id) 
	{
		return $this->getFieldOnID($id, 'title');
	}
	
	public function getMenu($where = '') 
	{
		return $this->getAllWithWhereAndOrder($where, 'wordstat', false);
	}
	
	public function getCompanyArrSortTitle() 
	{
		return $this->getAll('title', true);
	}
	
	public function getTitlesList()
	{
		return $this->getFields(array('id', 'title'));
	}
	
	public function getCompanyArrFromDeviceArr($device_arr, $fields = array('*'))
	{
		$ids = '';
		for ($i = 0; $i < count($device_arr); $i++)
		{
			$ids .= $device_arr[$i]['company_id'] . ', ';
		}
		$ids = '`id` IN (' . mb_substr($ids, 0, -2) . ')';
		
		return $this->getAllWithTheFieldsAndWhere($fields, $ids);
	}
}
?>