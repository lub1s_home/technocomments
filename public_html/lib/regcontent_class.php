<?php

require_once 'modules_class.php';

class RegContent extends Modules {
	
	public function __construct($db) {
		parent::__construct($db);
	}
	
	protected function getTitle() {
		return 'Регистрация на TechnoComments';		
	}
	
	protected function getDescription() {
		return 'Регистрация на TechnoComments';
	}
	
	protected function getKeyWords() {
		return 'регистрация, TechnoComments';
	}
	
	protected function getCanonical() {
		return 'registration';
	}
	
	protected function getMiddle() {

      
      
		// заменить пустые строки
		$sr['message'] = $this->getMessage();
		$sr['name_form_reg'] = $_SESSION['name_form_reg'];
		$sr['lastname_form_reg'] = $_SESSION['lastname_form_reg'];
		$sr['email_form_reg'] = $_SESSION['email_form_reg'];
		
		return $this->getReplaceTemplate($sr, 'reg');
	}
}
?>