<?php

require_once 'global_class.php';

class Device extends GlobalClass 
{
	public function __construct($db) 
	{
		parent::__construct('devices', $db);
	}
	
	public function addDevice($section_id, $company_id, $device) 
	{
		return $this->add(array('section_id' => $section_id, 'company_id' => $company_id, 'device' => $device));
	}
	
	public function editDeviceOnId($id, $section_id, $company_id, $device_name) 
	{
		return $this->editString(array('section_id' => $section_id, 'company_id' => $company_id, 'device' => $device_name), 'id', $id);
	}
	
	public function updateLastModified($id)
	{
		return $this->updateOnID($id, array('lastmod' => time()));
	}
	
	public function setReleaseDate($id, $date_unix)
	{
		return $this->updateOnID($id, array('release_date' => $date_unix));
	}
	
	public function getWhereForMenu($where) 
	{
		$menu = $this->getDistinctWithTheFieldsAndWhere(array('company_id'), $where);
		$string = '';
		for ($i = 0; $i < count($menu); $i++) {
			$string .= $menu[$i]['company_id'].',';
		}
		return '`id` IN ('.mb_substr($string, 0, -1).')';
	}
	
	// принимает массив с отзывами
	// возвращает массив с нужными устройствами
	public function getDeviceArrFromCommentsArr($comments_arr, $fields = array('*'))
	{
		$ids = '';
		for ($i = 0; $i < count($comments_arr); $i++)
		{
			$ids .= $comments_arr[$i]['device_id'] . ', ';
		}
		$ids = '`id` IN (' . mb_substr($ids, 0, -2) . ')';
		
		return $this->getAllWithTheFieldsAndWhere($fields, $ids);
	}
	
	public function editAffiliateLinkAliexpress($id, $link)
	{
		return $this->updateOnID($id, array('al_aliexpress' => $link));
	}
	
	public function editOzonId($device_id, $ozon_id)
	{
		return $this->updateOnID($device_id, array('al_ozon' => $ozon_id));
	}
}

?>