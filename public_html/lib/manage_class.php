<?php

mb_internal_encoding('UTF-8');

require_once 'characteristics_class.php';
require_once 'checkvalid_class.php';
require_once 'comments_class.php';
require_once 'company_class.php';
require_once 'config_class.php';
require_once 'device_class.php';
require_once 'mail_class.php';
require_once 'rating_class.php';
require_once 'section_class.php';
require_once 'user_class.php';
require_once 'video_class.php';

// модель, занимается низкоуровневыми операциями
class Manage {
	
	private $characteristics;
	private $comments;
	private $company;
	private $config;
	private $data;
	private $devices;
	private $mail;
	private $rating;
	private $sections;
	private $valid;
	private $user;
	private $video;
	
	public function __construct($db) {
		session_start();
		$this->characteristics = new Characteristics($db);
		$this->comments = new Comments($db);
		$this->company = new Company($db);
		$this->config = new Config();
		$this->data = $this->secureData(array_merge($_POST, $_GET));
		$this->devices = new Device($db);
		$this->mail = new Mail($db);
		$this->rating = new Rating($db);
		$this->sections = new Section($db);
		$this->user = new User($db);
		$this->valid = new CheckValid();
		$this->video = new Video($db);
	}
	
	private function secureData($data) {
		foreach($data as $key => $value) {
			if (is_array($value)) $this->secureData($value);
			else $data[$key] = htmlspecialchars($value);
		}
		return $data;
	}
	
	public function redirect($link) {
		header('Location: '.$link);
		exit();
	}
	
	public function regUser() {
		$link_reg = $this->config->address.'/registration#registration';
		
		$captcha = $this->data['captcha'];
		$_SESSION['name_form_reg'] = $this->data['name'];
		$_SESSION['lastname_form_reg'] = $this->data['lastname'];
		$_SESSION['email_form_reg'] = $this->data['email'];
		
		if (($_SESSION['captcha'] != $captcha) || ($_SESSION['captcha'] == '')) {
			return $this->returnMessage('CAPTCHA_ERROR', $link_reg);
		}

		$name = $this->data['name'];
		if ($name == '') return $this->returnMessage('NAME_EMPTY', $link_reg);
		
		$lastname = $this->data['lastname'];
		if ($lastname == '') return $this->returnMessage('LASTNAME_EMPTY', $link_reg);
		
		$email = $this->data['email'];
		if ($email == '') return $this->returnMessage('EMAIL_EMPTY', $link_reg);
		if (!$this->valid->validEmail($email)) return $this->returnMessage('EMAIL_ERROR', $link_reg);
		
		if ($this->user->isExistsUser($email)) return $this->returnMessage('EMAIL_EXISTS', $link_reg);
		
		$password = $this->data['password'];
		$re_password = $this->data['repeat_password'];
		if (($password == '') || ($re_password == '')) return $this->returnMessage('PASSWORD_EMPTY', $link_reg);
		if ($password !== $re_password) return $this->returnMessage('PASSWORD_NOT_EQUALLY', $link_reg); 
		$password = $this->hashPassword($password);
		
		$result = $this->user->addUser($name, $lastname, $email, $password, time());
		if ($result) {
			unset($_SESSION['name_form_reg']);
			unset($_SESSION['lastname_form_reg']);
			unset($_SESSION['email_form_reg']);
			
			// письмо с подтверждением на почту
			$this->mail->sendMailForConfirmEmail($email, $password);
			
			return $this->returnPageMessage('REG_SUCCESS', $this->config->address.'/message');
		}
		else return $this->unknownError($link_reg);
	}
	
	public function confirmEmail() {
		$hash = $this->data['confirm_email'];
		$email = $this->data['email'];
		$user = $this->user->getUserOnEmail($email);
		if ($hash == $user['password']) {
			$this->user->confirmEmail($user['id']);
			return $this->returnPageMessage('EMAIL_CONFIRM', $this->config->address.'/message');
		}
	}
	
	public function login() {
		$email = $this->data['email'];
		$_SESSION['form_auth_email'] = $email;
		$password = $this->data['password'];
		$password = $this->hashPassword($password);
		$r = $_SERVER['HTTP_REFERER'];
		$user = $this->user->checkUserAndGet($email, $password);
		if ($user) {
			
			//проверка активации учетной записи
			if (!$user['confirm_email']) {
				$_SESSION['error_auth'] = 2;
				return $r;
			}
			//если учетная запись активирована, то авторизоваться
			else {
				$_SESSION['email'] = $email;
				$_SESSION['password'] = $password;
				return $r;
			}
		}
		else {
			$_SESSION['error_auth'] = 1;
			return $r;
		}
	}

	
	public function logout() {
		unset($_SESSION['email']);
		unset($_SESSION['password']);
		unset($_SESSION['form_auth_email']);
		return $_SERVER['HTTP_REFERER'];
	}


	private function hashPassword($password) {
		return md5($password.$this->config->secret);
	}
	
	public function addComment() 
	{
		$link = $_SERVER['HTTP_REFERER'].'#add_review';
		$rs_data = $this->removeSpaces($this->data);
		
		$_SESSION['name_for_comment']     = $rs_data['name'];
		$_SESSION['comment_for_comment'] = $rs_data['comment'];
		$_SESSION['device_id_for_comment'] = $rs_data['device_id'];
		
		// проверяю наличие оценок пользователя
		for ($i = 0; $i < $rs_data['count_questions']; $i++) 
		{
			$_SESSION['rating_'.$i] = $rs_data['rating_'.$i];
		}
		
		$name = $rs_data['name'];
		if ($name == '') 
			return $this->returnMessage('NAME_EMPTY', $link);
		
		$comment = $rs_data['comment'];
		if ($comment == '') 
			return $this->returnMessage('COMMENT_EMPTY', $link);
		
		for ($i = 0; $i < $rs_data['count_questions']; $i++) 
		{
			if (!$rs_data['rating_'.$i]) 
				return $this->returnMessage('RATING_EMPTY', $link);
		}
		
		$device_id = $rs_data['device_id'];
		
		$coment_id = $this->comments->addComment($device_id, $comment, $name);
		if ($coment_id) {
			unset($_SESSION['name_for_comment']);
			unset($_SESSION['comment_for_comment']);
			
			// записываю оценки в базу
			$arr_insert_rating = array();
			for ($i = 0; $i < $rs_data['count_questions']; $i++) {
				$arr_insert_rating[$i] = array($coment_id, $rs_data['question_id_'.$i], $rs_data['rating_'.$i]);
			}
			$result_rating = $this->rating->addRating($arr_insert_rating);
			if ($result_rating) {
				for ($i = 0; $i < $rs_data['count_questions']; $i++) {
					unset($_SESSION['rating_'.$i]);
					unset($_SESSION['device_id_for_comment']);
				}
				return $this->returnMessage('COMMENT_TO_MODERATION', $link);
			}
			else return $this->unknownError($link);
		}
		else return $this->unknownError($link);
	}
	
	public function offer() 
	{
		$link = $_SERVER['HTTP_REFERER'] . '#reviews';
		
		$_SESSION['device_for_offer'] = $this->data['device'];
		$_SESSION['name_for_offer'] = $this->data['name'];
		$_SESSION['comment_for_offer'] = $this->data['comment'];
		
		
		// проверяю наличие оценок пользователя
		for ($i = 0; $i < $this->data['count_questions']; $i++) {
			$_SESSION['rating_'.$i] = $this->data['rating_'.$i];
		}
		
		$name = $this->data['name'];
		if ($name == '') 
			return $this->returnMessage('NAME_EMPTY', $link);
		
		$device = $this->data['device'];
		if ($device == '') 
			return $this->returnMessage('DEVICE_OFFER_EMPTY', $link);
		
		$comment = $this->data['comment'];
		if ($comment == '') 
			return $this->returnMessage('COMMENT_EMPTY', $link);
		
		
		for ($i = 0; $i < $this->data['count_questions']; $i++) 
		{
			if (!$this->data['rating_'.$i]) 
				return $this->returnMessage('RATING_EMPTY', $link);
			$rating .= $this->data['rating_'.$i] . ' ';
		}
		
		$section = $this->data['device_name_singular'];
		
		$this->mail->sendMailOffer($section, $device, $name, $comment, $rating);
		
		unset($_SESSION['device_for_offer']);
		unset($_SESSION['name_for_offer']);
		unset($_SESSION['comment_for_offer']);
		for ($i = 0; $i < $this->data['count_questions']; $i++) 
		{
			unset($_SESSION['rating_'.$i]);
		}
		return $this->returnMessage('COMMENT_TO_MODERATION', $link);
	}
	
	public function offerSaveDataAndRedirect()
	{
		$_SESSION['device_for_offer'] = $this->data['device'];
		$_SESSION['name_for_offer'] = $this->data['name'];
		$_SESSION['comment_for_offer'] = $this->data['comment'];
		
		return $this->data['link'];
	}
	
	public function filter() {
		// сбросить фильтр
		if ($this->data['filter'] == 'all') {
			$_SESSION['filter'] = array();
		}
		// назначить фильтр
		else {
			if ($_SESSION['filter'][ $this->data['filter'] ]) {
				unset($_SESSION['filter'][ $this->data['filter'] ]);
			}
			else {
				$_SESSION['filter'][ $this->data['filter'] ] = true;
			}
		}
		// если использую фильтр при нахождении не на первой странице, надо отсечь страницы
		// так выглядит код после ЧПУ
		$r = preg_replace('~/[0-9]+~', '', $_SERVER['HTTP_REFERER']);
		// так выглядел код до ЧПУ
		//$length = mb_strpos($_SERVER['HTTP_REFERER'], '&page=');
		//if ($length) $r = mb_substr($_SERVER['HTTP_REFERER'], 0, $length);
		//else $r = $_SERVER['HTTP_REFERER'];
		return $r;
		
	}
	
	private function removeSpaces($array) {
		foreach ($array as $key => $value) {
			$array[$key] = trim($array[$key]);
		}
		return $array;
	}
	
	private function unknownError($r) {
		return $this->returnMessage('UNKNOWN_ERROR', $r);
	}
	
	private function returnMessage($message, $r) {
		$_SESSION['message'] = $message;
		return $r;
	}
	
	private function returnPageMessage($message, $r) {
		$_SESSION['page_message'] = $message;
		return $r;
	}
	
	private function returnMessageWithIndex($message, $index, $r) {
		$_SESSION['message'] = $message;
		$_SESSION['message_index'] = $index;
		return $r;
	}
	
	
	
	/* Админка */
	
	public function adminAuth() {
		$login = $this->data['login'];
		$password = $this->data['password'];
		if (($login == $this->config->adm_login) && ($password == $this->config->adm_password)) {
			$_SESSION['adm_auth'] = true;
		}
		return $this->config->address.'/'.$this->config->adm_directory.'/';
	}
	
	public function adminLogout() {
		unset($_SESSION['adm_auth']);
		return $this->config->address.'/'.$this->config->adm_directory.'/';
	}
	
	public function adminEditCompany() 
	{
		if (($this->data['confirm'] == 'edit') OR ($this->data['confirm'] == 'редактировать'))
		{
			$id = $this->data['id'];
			$title = $this->data['title'];
			$site = $this->data['site'];
			$wordstat = $this->data['wordstat'];
			
			$this->company->editCompany($id, $title, $site, $wordstat);
		}
		return $_SERVER['HTTP_REFERER'] . '#company_' . $id;
	}
	
	public function adminAddCompany() 
	{
		$title = $this->data['title'];
		$site = $this->data['site'];
		$wordstat = $this->data['wordstat'];
		if (($this->valid->validWordstat($wordstat)) AND ($title))
		{
			$this->company->addCompany($title, $site, $wordstat);
		}
		return $_SERVER['HTTP_REFERER'];
	}
	
	public function adminDeleteCompany()
	{
		if (($this->data['confirm'] == 'delete') OR ($this->data['confirm'] == 'удалить'))
		{
			$this->company->deleteOnID($this->data['id']);
		}
		return $_SERVER['HTTP_REFERER'];
	}
	
	public function adminEditCharacteristics() 
	{
		$r = $_SERVER['HTTP_REFERER'];
		
		$id = $this->data['id']; // id характеристики
		$name = trim($this->data['edit_char_name']);
		$value = trim($this->data['edit_char_value']);
		
		$this->characteristics->editCharacteristic($id, $name, $value);
		return $r;
	}
	
	public function adminDeleteCharacteristic()
	{
		if (($this->data['confirm_delete_char'] == 'delete') OR ($this->data['confirm_delete_char'] == 'удалить'))
		{
			$this->characteristics->deleteOnID($this->data['id']);
		}
		return $_SERVER['HTTP_REFERER'];
	}
	
	public function adminAddCharacteristic() {
		$r = $_SERVER['HTTP_REFERER'];
		
		$device_id = $this->data['device_id'];
		
		$name = trim($this->data['name']);
		if ($name === '') {
			return $r;
		}
		
		$value = trim($this->data['value']);
		if ($value === '') {
			return $r;
		}
		
		$this->characteristics->addNewCharacteristic($device_id, $name, $value);
		return $r;
	}
	
	public function adminSelectAddPatternCharForDevice()
	{
		$redirect = $_SERVER['HTTP_REFERER'];
		$patternName = $this->data['pattern_name'];
		$deviceId = $this->data['device_id'];
		$arrChar = [];
		// доступные шаблоны
		switch ($patternName)
		{
			case 'смартфон': 
				$arrChar = array(
					'Процессор',
					'Графический ускоритель',
					'Оперативная память',
					'Внутренняя память',
					'Операционная система',
					'Аккумулятор',
					'Экран',
					'Основная камера',
					'Фронтальная камера',
					'4G',
					'Сканер отпечатка пальца',
					'Датчики',
					'Навигация',
					'Вес',
					'Размеры',
				);
				break;

			case 'мышь':
				$arrChar = array(
					'Вес',
					'Размеры',
					'Длина кабеля',
					'Частота опроса',
					'Количество кнопок',
					'Тип сенсора',
					'Сенсор',
					'Разрешение',
					'Переключатели клавиш',
					'Долговечность переключателей',
					'Программное обеспечение',
					'Подсветка',
				);
				break;
		}
		for ($i = 0; $i < count($arrChar); $i++) {
			$this->characteristics->addNewCharacteristic($deviceId, $arrChar[$i], '*');
		}
		return $redirect;
	}
	
	public function adminAddDevice() {
		$r_error = $_SERVER['HTTP_REFERER'];
		
		$section_id = $this->data['section_id'];
		$company_id = $this->data['company_id'];
		$device = $this->data['name'];
		
		if ((!$section_id) || (!$company_id) || (!$device)) {
			return $r_error;
		}
		
		$result = $this->devices->addDevice($section_id, $company_id, $device);
		if ($result) {
			$last_id = $this->devices->getLastId();
			$r = $this->config->address.'/'.$this->config->adm_directory.'/?view=device&id='.$last_id;
			return $r;
		}
		
		return $r_error;
	}
	
	public function adminEditDevice() {
	
		$id = $this->data['device_id'];
		$section_id = $this->data['section_id'];
		$company_id = $this->data['company_id'];
		$device_name = $this->data['name'];
		
		$this->devices->editDeviceOnId($id, $section_id, $company_id, $device_name);
		return $_SERVER['HTTP_REFERER'];
	}
	
	public function adminUpdateDeviceLastmod()
	{
		if (($this->data['confirm'] == 'update') OR ($this->data['confirm'] == 'обновить'))
		{
			$this->devices->updateLastModified($this->data['id']);
		}
		return $_SERVER['HTTP_REFERER'];
	}
	
	public function adminEditDeviceReleaseDate()
	{
		$r = $_SERVER['HTTP_REFERER'];
		if (($this->data['confirm'] == 'edit') OR ($this->data['confirm'] == 'редактировать'))
		{
			if (!$this->data['date'])
				return $r;
			
			$date_unix = strtotime($this->data['date']);
			$this->devices->setReleaseDate($this->data['id'], $date_unix);
		}
		return $r;
	}
	
	public function adminEditAffiliateLinkAliexpress()
	{
		$this->devices->editAffiliateLinkAliexpress($this->data['device_id'], $this->data['al_aliexpress']);
		return $_SERVER['HTTP_REFERER'] . '#affiliate_link';
	}
	
	public function adminEditOzonId()
	{
		$r = $_SERVER['HTTP_REFERER'] . '#ozon_id';
		$ozon_id = trim($this->data['al_ozon']);
		
		if (!$this->valid->validID($ozon_id) && $ozon_id)
			return $r;
		$this->devices->editOzonId($this->data['device_id'], $ozon_id);
		return $r;
		
	}
	
	public function adminApproveComment() {
		
		$r = $_SERVER['HTTP_REFERER'];
		if ($this->data['confirm_delete'] != '') return $r;
		
		$id = $this->data['comment_id'];
		$comment = $this->data['comment'];
		$name = $this->data['name'];
		
		$this->comments->admApproveComment($id, $comment, $name);
		$this->devices->updateLastModified($this->data['device_id']);
		
		return $r;
	}
	
	public function adminDeleteComment() 
	{
		$this->data['confirm_delete'] = mb_strtolower($this->data['confirm_delete']);
		if (($this->data['confirm_delete'] == 'delete') || ($this->data['confirm_delete'] == 'удалить')) 
		{
			$this->comments->deleteOnid($this->data['comment_id']);
			$this->rating->delete('`comment_id` = '.$this->data['comment_id']);
			$this->devices->updateLastModified($this->data['device_id']);
		}
		return $_SERVER['HTTP_REFERER'];
	}
	
	public function adminEditPhoto() 
	{
		$r = $_SERVER['HTTP_REFERER'] . '#edit_photo';
		
		$file = $_FILES['photo'];
		if ($file['error'])
			return $r;
			
		if ($file['type'] != 'image/jpeg') 
			return $r;
		
		if (!file_exists($this->data['photo_path']))
		{
			mkdir($this->data['photo_path'], 0777, true);
		}
		move_uploaded_file( $file['tmp_name'], $this->data['photo_path'] . $this->data['photo_name'] );
		
		return $r;
	}
	
	public function adminDeletePhoto()
	{
		if (($this->data['confirm_delete'] == 'delete') OR ($this->data['confirm_delete'] == 'удалить'))
		{
			if (file_exists($this->data['photo_path'] . $this->data['photo_name']))
			{
				unlink($this->data['photo_path'] . $this->data['photo_name']);
			}
		}
		return $_SERVER['HTTP_REFERER'] . '#edit_photo';
	}
	
	public function adminAddVideo() 
	{
		$r = $_SERVER['HTTP_REFERER'];
		$source = $this->data['source'];
		$device_id = $this->data['device_id'];
		
		if (!$source) return $this->returnMessageWithIndex('ADMIN_VIDEO_SOURCE_EMPTY', 'add_video', $r.'#add_video');
		
		$this->video->addVideo($device_id, $source);
		return $r . '#edit_or_delete_video';
	}
	
	public function adminEditVideo() 
	{
		$r = $_SERVER['HTTP_REFERER'].'#edit_or_delete_video';
		$video_id = $this->data['video_id'];
		$new_source = $this->data['source'];
		$this->data['confirm'] = mb_strtolower($this->data['confirm']);
		
		if (($this->data['confirm'] == 'редактировать') || ($this->data['confirm'] == 'edit')) 
		{
			if (!$new_source) return $this->returnMessageWithIndex('ADMIN_VIDEO_SOURCE_EMPTY', 'edit_or_delete_video_err', $r);
			
			$result = $this->video->editVideo($video_id, $new_source);
			if ($result) return $this->returnMessageWithIndex('ADMIN_VIDEO_EDIT_SUCCESS', 'edit_or_delete_video_suc', $r);
		}
		else return $this->returnMessageWithIndex('ADMIN_VIDEO_NOT_CONFIRM', 'edit_or_delete_video_err', $r);
	}
	
	public function adminDeleteVideo() {
		$r = $_SERVER['HTTP_REFERER'].'#edit_or_delete_video';
		$video_id = $this->data['video_id'];
		$this->data['confirm'] = mb_strtolower($this->data['confirm']);
		if (($this->data['confirm'] == 'удалить') || ($this->data['confirm'] == 'delete')) {
			$result = $this->video->deleteOnId($video_id);
			if ($result) return $this->returnMessageWithIndex('ADMIN_VIDEO_DELETE_SUCCESS', 'edit_or_delete_video_suc', $r);
		}
		else return $this->returnMessageWithIndex('ADMIN_VIDEO_NOT_CONFIRM', 'edit_or_delete_video_err', $r);
	}
}

?>