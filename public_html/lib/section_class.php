<?php

require_once 'global_class.php';

class Section extends GlobalClass 
{
	public function __construct($db) 
	{
		parent::__construct('sections', $db);
	}
	
	public function getSingular($id) 
	{
		return $this->getFieldOnID($id, 'name_singular');
	}
	
	public function setPlural($id, $new_value) 
	{
		return $this->setFieldOnID($id, 'name_plural', $new_value);
	}
	
	public function setSingular($id, $new_value) 
	{
		return $this->setFieldOnID($id, 'name_singular', $new_value);
	}
	
	public function setGenitive($id, $new_value) 
	{
		return $this->setFieldOnID($id, 'name_genitive', $new_value);
	}
	
	public function setPrepositional($id, $new_value) 
	{
		return $this->setFieldOnID($id, 'name_prepositional', $new_value);
	}
	
	public function setLink($id, $new_value) 
	{
		return $this->setFieldOnID($id, 'for_link', $new_value);
	}
	
	public function addSection($plural, $singular, $genitive, $prepositional, $link) 
	{
		return $this->add(array('name_plural' => $plural, 'name_singular' => $singular, 'name_genitive' => $genitive, 'name_prepositional' => $prepositional, 'for_link' => $link));
	}
	
	public function getAllLinks() 
	{
		return $this->getAllWithTheFields(array('id', 'for_link'));
	}
  
    public function getMenu() 
	{
		return $this->getAllWithWhereAndOrder('', 'name_plural', true);
	}
	
	public function getGlobalMenu()
	{
		return $this->getAll('wordstat', false);
	}
	
	public function getSectionArrFromDeviceArr($device_arr, $fields = array('*'))
	{
		$ids = '';
		for ($i = 0; $i < count($device_arr); $i++)
		{
			$ids .= $device_arr[$i]['section_id'] . ', ';
		}
		$ids = '`id` IN (' . mb_substr($ids, 0, -2) . ')';
		
		return $this->getAllWithTheFieldsAndWhere($fields, $ids);
	}
}

?>