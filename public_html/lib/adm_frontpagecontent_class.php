<?php

require_once 'adm_modules_class.php';
require_once 'comments_class.php';

require_once 'device_class.php';
require_once 'company_class.php';
require_once 'section_class.php';

class Adm_FrontPageContent extends Adm_Modules 
{
	private $comments;
	private $dev_obj;
	private $com_obj;
	private $sec_obj;
	
	public function __construct($db) 
	{
		parent::__construct($db);
		
		$this->comments = new Comments($db);
		$this->dev_obj = new Device($db);
		$this->com_obj = new Company($db);
		$this->sec_obj = new Section($db);
	} 
	
	protected function getTitle() 
	{
		return 'Отзывы';
	}
	
	protected function getMiddle() 
	{
		$unmod_comments = $this->comments->getAllUnmoderatedComments();
			//print_r($unmod_comments);
		
		if ($unmod_comments) 
		{
			// получаю массив нужных девайсов
			$dev_arr = $this->dev_obj->getDeviceArrFromCommentsArr($unmod_comments, array('id', 'section_id', 'company_id', 'device'));
			
			// получаю массив нужных секций
			$sec_arr = $this->sec_obj->getSectionArrFromDeviceArr($dev_arr, array('id', 'name_singular', 'for_link'));
			// фикс - ключи айдишники для секций
			for ($i = 0; $i < count($sec_arr); $i++)
			{
				$fix_sec_arr[ $sec_arr[$i]['id'] ]['name'] = $sec_arr[$i]['name_singular'];
				$fix_sec_arr[ $sec_arr[$i]['id'] ]['link'] = $sec_arr[$i]['for_link'];
			}
			
			// получаю массив нужных брендов
			$com_arr = $this->com_obj->getCompanyArrFromDeviceArr($dev_arr, array('id', 'title'));
			// фикс - ключи айдишники для брендов
			// print_r($com_arr);
			for ($i = 0; $i < count($dev_arr); $i++) {
				if (!isset($com_arr[$i])) {
					break;
				}
				$fix_com_arr[ $com_arr[$i]['id'] ]['name'] = $com_arr[$i]['title'];
				$fix_com_arr[ $com_arr[$i]['id'] ]['link'] = mb_strtolower(str_replace(' ', '_', $com_arr[$i]['title']));
			}
			
			// фикс - ключи айдишники для девайсов (это готовый массив с полными данными)
			//print_r($dev_arr);
			for ($i = 0; $i < count($dev_arr); $i++)
			{
				$fix_dev_arr[ $dev_arr[$i]['id'] ]['name'] = $fix_sec_arr[ $dev_arr[$i]['section_id'] ]['name'] . ' ' . $fix_com_arr[ $dev_arr[$i]['company_id'] ]['name'] . ' ' . $dev_arr[$i]['device'];
				$fix_dev_arr[ $dev_arr[$i]['id'] ]['link'] = $fix_sec_arr[ $dev_arr[$i]['section_id'] ]['link'] . '/' . $fix_com_arr[ $dev_arr[$i]['company_id'] ]['link'] . '/' . mb_strtolower(str_replace(' ', '_', $dev_arr[$i]['device']));
			}
			
			$comm_snippets = '';
			for ($i = 0; $i < count($unmod_comments); $i++) 
			{
              	$sr['device_link_anchor'] = $fix_dev_arr[ $unmod_comments[$i]['device_id'] ]['name'];
				$sr['device_link'] = $fix_dev_arr[ $unmod_comments[$i]['device_id'] ]['link'];
				
				$sr['name'] = $unmod_comments[$i]['name'];
				$sr['comment_id'] = $unmod_comments[$i]['id'];
				$sr['comment'] = $unmod_comments[$i]['comment'];
				$sr['info'] = 'id: ' . $unmod_comments[$i]['id'];
				$sr['device_id'] = $unmod_comments[$i]['device_id'];
				
				$comm_snippets .= $this->getReplaceTemplate($sr, 'adm_comments_moderation_snippets');
			}
		}
		else 
		{
			$comm_snippets = 'Новых отзывов нет';
		}
		$sr_page['page_name'] = 'Новые отзывы';
		return $this->getReplaceTemplate($sr_page, 'adm_page_name') . $comm_snippets;
	}

}

?>