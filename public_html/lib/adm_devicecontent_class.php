<?php

require_once 'adm_modules_class.php';
require_once 'company_class.php';
require_once 'characteristics_class.php';
require_once 'device_class.php';
require_once 'section_class.php';
require_once 'video_class.php';

class Adm_DeviceContent extends Adm_Modules 
{
	private $company;
	private $characteristics;
	private $devices;
	private $sections;
	private $video;
	
	public function __construct($db) 
	{
		parent::__construct($db);
		
		$this->company = new Company($db);
		$this->characteristics = new Characteristics($db);
		$this->devices = new Device($db);
		$this->sections = new Section($db);
		$this->video = new Video($db);
	} 
	
	protected function getTitle() 
	{
		return 'Устройства';
	}
	
	protected function getMiddle() 
	{	
		$company = $this->company->getCompanyArrSortTitle();
		$section = $this->sections->getMenu();
			
		//дополнительный массив, чтобы не было ошибки при нарушенной последовательности id-шников в таб. "Sections"
		$arr_sec = array();
		for ($i = 0; $i < count($section); $i++) 
		{
			$arr_sec[ $section[$i]['id'] ] = array('name_singular' => $section[$i]['name_singular'], 'for_link' => $section[$i]['for_link']);
		}
		
		//дополнительный массив, чтобы не было ошибки при нарушенной последовательности id-шников в таб. "Company"
		$arr_com_title = array();
		for ($i = 0; $i < count($company); $i++) 
		{
			$arr_com_title[ $company[$i]['id'] ] = $company[$i]['title'];
		}
		
		// вывожу выбранное устройство
		if (isset($this->data['id']))
		{
			$device = $this->devices->get($this->data['id']);
			
			$sr['adm_device_edit'] = $this->getFormEditDevice($device, $section, $company);
			$sr['name'] = $arr_sec[ $device['section_id'] ]['name_singular'].' '.$arr_com_title[ $device['company_id'] ].' '.$device['device'];
			$sr['lastmod'] = $this->formatDate($device['lastmod']);
			
			$sr['release_date'] = date('Y-m-d', $device['release_date']);
			
			$sr['characteristics'] = $this->getCharacteristics();
			
			//если характеристики еще не внесены, надо предложить шаблон автозаполнения полей
			if (!$sr['characteristics'])
				$sr['select_pattern_char'] = $this->getTemplate('adm_device_select_pattern_char');
			else
				$sr['select_pattern_char'] = '';
			
			$sr['message_edit_or_delete_video_err'] = ($message_edit_or_delete_video = $this->getMessage('edit_or_delete_video_err'))? $this->getReplaceTemplate(array('message' => $message_edit_or_delete_video), 'adm_message_red') : '';
			$sr['message_edit_or_delete_video_suc'] = ($message_edit_or_delete_video = $this->getMessage('edit_or_delete_video_suc'))? $this->getReplaceTemplate(array('message' => $message_edit_or_delete_video), 'adm_message_green') : '';
			
			$sr['all_video'] = $this->getAllVideo();
			
			$sr['message_add_video'] = ($message_add_video = $this->getMessage('add_video'))? $this->getReplaceTemplate(array('message' => $message_add_video), 'adm_message_red') : '';
			
			$sr['device_id'] = $this->data['id'];
			
			// редактирование/загрузка фото
			$sr['edit_photo'] = '';
			for ($i = 1; $i < 6; $i++)
			{
				$photo_sr['image_path'] = $this->config->dir_img . $arr_sec[ $device['section_id'] ]['for_link'] . '/' . str_replace(' ', '_', mb_strtolower($arr_com_title[ $device['company_id'] ])) . '/' . mb_strtolower(str_replace(' ', '_', $device['device'])) . '/';
				$photo_sr['image_name'] = $i . '.jpg';
				
				$sr['edit_photo'] .= $this->getReplaceTemplate($photo_sr, 'adm_edit_device_photo');
			}
			
			$sr['al_ozon'] = ($device['al_ozon']) ? 
				$device['al_ozon'] : 
				'';
			$sr['al_aliexpress'] = $device['al_aliexpress'];
				
			return $this->getReplaceTemplate($sr, 'adm_device');
		}
		// вывожу все устройства
		else 
		{
			$devices = $this->devices->getAll('id', false);
			
			$device_snippets = '';
			for ($i = 0; $i < count($devices); $i++) 
			{
				$name_section = $arr_sec[ $devices[$i]['section_id'] ]['name_singular'];
				$name_company = $arr_com_title[ $devices[$i]['company_id'] ];
				
				$sr_device['id'] = $devices[$i]['id'];
				$sr_device['title'] = $name_section.' '.$name_company.' '.$devices[$i]['device'];
				$sr_device['link'] = '?view=device&amp;id='.$devices[$i]['id'];
				
				//проверяю наличие партнерки Озон
				if ($devices[$i]['al_ozon'])
				{
					$sr_device['ozon_check'] = 'ok';
					$sr_device['ozon_check_class'] = 'al_ok';
				}
				else
				{
					$sr_device['ozon_check'] = 'not';
					$sr_device['ozon_check_class'] = 'al_not';
				}
				
				//проверяю наличие партнерки Али
				if ($devices[$i]['al_aliexpress'])
				{
					$sr_device['aliexpress_check'] = 'ok';
					$sr_device['aliexpress_check_class'] = 'al_ok';
				}
				else
				{
					$sr_device['aliexpress_check'] = 'not';
					$sr_device['aliexpress_check_class'] = 'al_not';
				}
				
				$device_snippets .= $this->getReplaceTemplate($sr_device, 'adm_device_snippet');
			}
			
			$sr['add_device'] = $this->getFormAddDevice($section, $company);
			$sr['device_snippets'] = $device_snippets;
			return $this->getReplaceTemplate($sr, 'adm_device_main');
		}
	}
	
	private function getAllVideo() {
		$video = $this->video->getVideoForDevice($this->data['id']);
		if ($video) 
		{
			$video_snippet = '';
			for ($i = 0; $i < count($video); $i++) 
			{
				$sr['video_id'] = $video[$i]['id'];
				$sr['source'] = $video[$i]['source'];
				$video_snippet .= $this->getReplaceTemplate($sr, 'adm_device_video_all');
			}
			return $video_snippet;
		}
		else 
		{
			$sr['message'] = 'отсутствуют';
			return $this->getReplaceTemplate($sr, 'adm_message_red');
		} 
	}
	
	private function getCharacteristics() 
	{
		$ch = $this->characteristics->getAllOnField('device_id', $this->data['id']);
		$text = '';
		for ($i = 0; $i < count($ch); $i++) 
		{
			$sr['id'] = $ch[$i]['id'];
			$sr['name'] = $ch[$i]['name'];
			$sr['value'] = $ch[$i]['value'];
			$text .= $this->getReplaceTemplate($sr, 'adm_characteristics_snippet');
		}
		return $text;
	}
	
	private function getFormAddDevice($sections, $company) 
	{
		$select_section = '';
		for ($i = 0; $i < count($sections); $i++) 
		{
			$sec_sr['value'] = $sections[$i]['id'];
			$sec_sr['name'] = $sections[$i]['name_singular'];
			$select_section .= $this->getReplaceTemplate($sec_sr, 'adm_device_add_select');
		}
		$sr['select_section'] = $select_section;
		
		$select_company = '';
		for ($i = 0; $i < count($company); $i++) 
		{
			$com_sr['value'] = $company[$i]['id'];
			$com_sr['name'] = $company[$i]['title'];
			$select_company .= $this->getReplaceTemplate($com_sr, 'adm_device_add_select');
		}
		$sr['select_company'] = $select_company;
		
		return $this->getReplaceTemplate($sr, 'adm_device_add');
	}
	
	private function getFormEditDevice($device, $section, $company)
	{
		$select_section = '';
		for ($i = 0; $i < count($section); $i++) 
		{
			$sec_sr['value'] = $section[$i]['id'];
			$sec_sr['name'] = $section[$i]['name_singular'];
			if ($device['section_id'] == $section[$i]['id']) 
			{
				$sec_sr['selected'] = 'selected';
			}
			else 
			{
				$sec_sr['selected'] = '';
			}	
			$select_section .= $this->getReplaceTemplate($sec_sr, 'adm_device_edit_select');
		}
		$sr['select_section'] = $select_section;
		
		$select_company = '';
		for ($i = 0; $i < count($company); $i++) 
		{
			$com_sr['value'] = $company[$i]['id'];
			$com_sr['name'] = $company[$i]['title'];
			if ($device['company_id'] == $company[$i]['id']) 
			{
				$com_sr['selected'] = 'selected';
			}
			else 
			{
				$com_sr['selected'] = '';
			}	
			$select_company .= $this->getReplaceTemplate($com_sr, 'adm_device_edit_select');
		}
		$sr['select_company'] = $select_company;
		
		$sr['device_id'] = $device['id'];
		$sr['device_name'] = $device['device'];
		
		return $this->getReplaceTemplate($sr, 'adm_device_edit');
	}
}
?>