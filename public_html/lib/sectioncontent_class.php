<?php
require_once 'comments_class.php';
require_once 'modules_class.php';
require_once 'rating_class.php';

class SectionContent extends Modules
{
	private $comments;
	private $rating;
	private $info;
	private $page; // номер текущей страницы
	
	public function __construct($db)
	{
		parent::__construct($db);
		$this->comments = new Comments($db);
		$this->rating  = new Rating($db);
		
		$condition_device = '';
		//Если type пустой, то это главная страница
		if (isset($this->data['type'])) {
			if ($this->modules_info['sec_id']) {
				$this->info['SEO']['sec_plural'] = $this->modules_info['sec_ar'][ $this->modules_info['iterator_for_sec_ar'] ]['name_plural'];
			} else {
				header('HTTP/1.0 404 Not Found');
				$this->notfound = true;
				return;
			}
			// получаю условие для выборки устройств из базы
			$condition_device = '(`section_id` = ' . $this->modules_info['sec_id'] . ')';
		}
		
		// корректность номера страницы
		if (isset($this->data['page'])) {
			if ($this->valid->validPageNumber($this->data['page'])) {
				$this->page = $this->data['page'];
			} else {
				header('HTTP/1.0 404 Not Found');
				$this->notfound = true;
				return;
			}
		} else {
			$this->page = 1;
		}
		
		// если выбран бренд, проверяю его на корректность
		// в случае успеха, записываю бренд в info->SEO->brand_name
		$this->info['SEO']['brand_name'] = '';
		if (isset($this->data['filter'])) {
			$address_bar_brand = str_replace('_', ' ', $this->data['filter']);
			for ($i = 0; $i < count($this->modules_info['com_ar']); $i++) {
				if ($address_bar_brand == mb_strtolower($this->modules_info['com_ar'][$i]['title'])) {
					$this->info['SEO']['brand_name'] = ' '.$this->modules_info['com_ar'][$i]['title'];
					$this->info['com_id'] = $this->modules_info['com_ar'][$i]['id'];
				}
			}
			// если в адресной строке передается бренд, которого не существует - 404 ошибка
			if (!$this->info['SEO']['brand_name']) {
				header('HTTP/1.0 404 Not Found');
				$this->notfound = true;
				return;
			}
			// дополняю условие для выборки устройств из базы
			$condition_device .= ' AND (`company_id` = ' . $this->info['com_id'] . ')';
		}
		
		// проверяю, если ли выборка устройств для текущих секции, бренда, номера страницы и получаю массив устройств
		$limit_begin = ($this->page - 1) * $this->config->count_blog;
		$limit = $limit_begin . ', ' . $this->config->count_blog;
		$this->info['dev_ar'] = $this->dev_obj->getFewFreshString($condition_device, $limit);
		// если массив устройств не сформировался - 404 ошибка
		if (!$this->info['dev_ar']) {
			header('HTTP/1.0 404 Not Found');
			$this->notfound = true;
			return;
		}
		
		// Last-Modified
		$lastmod = 0;
		for ($i = 0; $i < count($this->info['dev_ar']); $i++) {
			if ($this->info['dev_ar'][$i]['lastmod'] > $lastmod) {
				$lastmod = $this->info['dev_ar'][$i]['lastmod'];
			}
		}
		$last_modified = gmdate("D, d M Y H:i:s \G\M\T", $lastmod);
		$if_modified_since = false;
		if (isset($_ENV['HTTP_IF_MODIFIED_SINCE'])) {
			$if_modified_since = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));
		}
		if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
			$if_modified_since = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
		}
		if ($if_modified_since && $if_modified_since >= $lastmod) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
			exit;
		}
		header('Last-Modified: '. $last_modified);
		
		
		// получаю количество устройств для текущих секции, бренда (для постраничной навигации)
		$this->info['count_dev_filter'] = $this->dev_obj->getCount($condition_device);
	}
	
	protected function getTitle()
	{
		if ($this->notfound === false) {
			$page = ($this->page == 1)? '' : ' - страница '.$this->page;
			if (isset($this->data['type'])) {
				// если это страница раздела
				return $this->info['SEO']['sec_plural'] . $this->info['SEO']['brand_name'] . ' - отзывы' . $page;
			} else {
				// если это главная страница
				return 'Отзывы о компьютерной технике' . $page;
			}
		} else {
			return 'Страница не найдена - ошибка 404';
		}
	}
	
	protected function getDescription()
	{
		if ($this->notfound === false) {
			$page = ($this->page == 1)
				? ''
				: ' - страница '.$this->page;
			// если страница раздела
			if (isset($this->data['type'])) {
				if (isset($this->data['filter'])) {
					// если страница раздела с брендом
					return $this->info['SEO']['sec_plural'] . $this->info['SEO']['brand_name'] .
						': реальные отзывы, оценки, технические характеристики, ' .
						'обзоры, мнения владельцев, фото, видео' . $page . '.';
				} else {
					// если страница раздела без бренда
					return $this->info['SEO']['sec_plural'] .
						': новинки, реальные отзывы, оценки, технические характеристики, ' .
						'обзоры, мнения владельцев, фото, видео' . $page . '.';
				}
			} else {
				// если главная страница
				return 'TechnoComments - отзывы о компьютерной, мобильной технике, периферии и комплектующих. ' .
					'Реальные мнения владельцев' . $page . '.';
			}
		} else {
			return 'Страница не найдена - ошибка 404';
		}
	}
	
	protected function getKeyWords()
	{
		if ($this->notfound === false) {
			if (isset($this->data['type'])) {
				// если это страница раздела
				return mb_strtolower($this->info['SEO']['sec_plural']) .
					mb_strtolower($this->info['SEO']['brand_name']) .
					', отзывы, мнения, новинки, фото, видео, обзоры, технические характеристики';
			} else {
				// если это главная страница
				return 'компьютерная техника, устройства, IT, периферия, комплектующие, ' .
					'отзывы, мнения, новинки, фото, видео, обзоры, технические характеристики';
			}
		} else {
			return 'Страница не найдена - ошибка 404';
		}
	}
	
	protected function getCanonical()
	{
		if ($this->notfound === false) {
			$page = ($this->page == 1)
				? ''
				: '/' . $this->page;
			if (!isset($this->data['type'])) {
				return $page;
			}
			$brand = (isset($this->data['filter']))
				? '/' . $this->data['filter']
				: '';
			return '/' . $this->data['type'] . $brand . $page;
		} else {
			return '/notfound';
		}
	}
	
	protected function getMiddle()
	{
		if ($this->notfound === false) {
			$main_sr['type_device'] = (isset($this->info['SEO']['sec_plural']))
				? $this->info['SEO']['sec_plural']
				: 'Компьютерная техника';
			
			// получаю список устройств на текущую страницу
			$device = $this->info['dev_ar'];
			
			// вытаскиваю id-шники из $device, чтобы вытащить количество комментариев только по нужным id
			$where_for_s_i_section = '';
			for ($i = 0; $i < count($device); $i++) {
				$where_for_s_i_section .= $device[$i]['id'].',';
			}
			$where_for_s_i_section = substr($where_for_s_i_section, 0, -1);
			$where_for_s_i_section = ' AND (device_id IN (' . $where_for_s_i_section . '))';
			
			//дополнительный массив, чтобы не было ошибки при нарушенной последовательности id-шников в таб. "Sections"
			$arr_sec = array();
			for ($i = 0; $i < count($this->modules_info['sec_ar']); $i++) {
				$arr_sec[ $this->modules_info['sec_ar'][$i]['id'] ] = array(
					$this->modules_info['sec_ar'][$i]['name_singular'],
					$this->modules_info['sec_ar'][$i]['for_link']
				);
			}
			
			//дополнительный массив, чтобы не было ошибки при нарушенной последовательности id-шников в таб. "Company"
			$arr_com_title = array();
			for ($i = 0; $i < count($this->modules_info['com_ar']); $i++) {
				$arr_com_title[ $this->modules_info['com_ar'][$i]['id'] ] = $this->modules_info['com_ar'][$i]['title'];
			}
			
			// считаю количество отзывов и записываю в массив по принципу
			$arr_s_i = $this->comments->getSmallInfoForSection($where_for_s_i_section);
			$where_for_rating = '';
			for ($i = 0; $i < count($arr_s_i); $i++) {
				// для подсчета рейтинга
				$where_for_rating .= $arr_s_i[$i]['id'].',';
				$arr_for_rating_gBy_deviceId[ $arr_s_i[$i]['device_id'] ][] = $arr_s_i[$i]['id'];
			}
			$where_for_rating = substr($where_for_rating, 0, -1);
				//print_r($arr_for_rating_gBy_deviceId); // массив содержит id комментариев
			
			if (($where_for_rating) && ($arr_for_rating_gBy_deviceId)) {
				$count_rating = $this->rating->getCountRatingOnDevices($where_for_rating, $arr_for_rating_gBy_deviceId);
			}
			
			// формирую сниппеты устройств
			$device_snippets = '';
			for ($i = 0; $i < count($device); $i++) {
				$name_section_rus = $arr_sec[ $device[$i]['section_id'] ][0];
				$name_company = $arr_com_title[ $device[$i]['company_id'] ];
				
				$sr_device_snippet['name_type'] = $name_section_rus;
				$sr_device_snippet['name_company'] = $name_company;
				$sr_device_snippet['name_device'] = $device[$i]['device'];
				
				if (isset($count_rating[ $device[$i]['id'] ])) {
					$sr_device_snippet['CSS_height'] = round($count_rating[ $device[$i]['id'] ] * 10);
					$count_rating_value = round($count_rating[ $device[$i]['id'] ], 1);
				} else {
					$sr_device_snippet['CSS_height'] = 0;
					$count_rating_value = 0;
				}
				$sr_device_snippet['CSS_position'] = $sr_device_snippet['CSS_height'] - 50;
				
				if (mb_strlen($count_rating_value) == 1) {
					$sr_device_snippet['first_digit'] = $count_rating_value;
					$sr_device_snippet['last_digit'] = 0;
				} else {
					$sr_device_snippet['first_digit'] = mb_substr($count_rating_value, 0, 1);
					$sr_device_snippet['last_digit'] = mb_substr($count_rating_value, 2, 1);
				}
				
				$name_section_eng = $arr_sec[ $device[$i]['section_id'] ][1];
				
				$sr_device_snippet['link'] = $name_section_eng . '/' .
					$this->replaceSpaces($name_company) . '/' .
					$this->replaceSpaces($device[$i]['device']);
				
				$sr_device_snippet['image'] = $this->config->address . '/' .
					$this->config->dir_img.$name_section_eng . '/' .
					$this->replaceSpaces($name_company) . '/' .
					$this->replaceSpaces($device[$i]['device']) . '/1.jpg';
				
				$device_snippets .= $this->getReplaceTemplate($sr_device_snippet, 'section_device_snippet');
			}
			
			// цепочка навигации
			if (isset($this->data['type'])) {
				$sr_bread_crumb['bread_crumb_link'] = '/';
				$sr_bread_crumb['bread_crumb_name'] = 'Главная';
				$main_sr['bread_crumb'] = $this->getReplaceTemplate($sr_bread_crumb, 'section_bread_crumb');
				if (isset($this->data['filter'])) {
					$sr_bread_crumb['bread_crumb_link'] = '/' . $this->data['type'];
					$sr_bread_crumb['bread_crumb_name'] = $main_sr['type_device'];
					$main_sr['bread_crumb'] .= ' ' .
						$this->getReplaceTemplate($sr_bread_crumb, 'section_bread_crumb') . ' ' .
						$this->info['SEO']['brand_name'];
				} else {
					$main_sr['bread_crumb'] .= ' ' . $main_sr['type_device'];
				}
			} else {
				$main_sr['bread_crumb'] = 'Главная';
			}
			
			//получаю верстку всей страницы
			$main_sr['offer_or_description'] = $this->getOfferOrDescription();
			$main_sr['brand'] = $this->info['SEO']['brand_name'];
			$main_sr['page'] = ($this->page == 1)
				? ''
				: ' - страница '.$this->page;
			$main_sr['section_device_snippets'] = $device_snippets;
			//если я на главной, то не добавлять слеш в ссылку, а если в разделе, то добавлять
			$pagi_link = (isset($this->data['type']))
				? $this->config->address . '/' . $this->data['type']
				: $this->config->address;
				
			$main_sr['pagination'] = $this->getPagination($this->info['count_dev_filter'], $pagi_link, $this->page);

			return $this->getReplaceTemplate($main_sr, 'section');
		} else {
			return $this->getTemplate('not_found');
		}
	}
	
	private function getPagination($count_elements, $link, $current_page)
	{
		$count_pages = ceil($count_elements / $this->config->count_blog);
		if ($count_pages > 1) {
			$pages = '';
			if ($count_pages > 5) {
				if (($current_page == 1) || ($current_page == 2) || ($current_page == 3)) {
					$begin = 1;
					$end = 5;
				} else {
					if ($current_page > 3) {
						//добавляю ссылку на первую страницу
						$sr['class'] = '';
						$sr['number'] = 1;
						$sr['link'] = $link;
						$pages = $this->getReplaceTemplate($sr, 'pagination_number') .'... ';
						
						if ($current_page == $count_pages) {
							$begin = $current_page - 4;
							$end = $current_page;
						} elseif (($current_page + 1) == $count_pages) {
							$begin = $current_page - 3;
							$end = $current_page + 1;
						} else {
							$begin = $current_page - 2;
							$end = $current_page + 2;
						}
					}
				}
			} else {
				$begin = 1;
				$end = $count_pages;
			}
			
			for ($i = $begin; $i <= $end; $i++) {
				$sr['class'] = ($current_page == $i)
					? 'class=\'current\''
					: '';
				$sr['number'] = $i;
				$sr['link'] = ($i == 1)
					? $link
					: $link . '/' . $i;
				$pages .= $this->getReplaceTemplate($sr, 'pagination_number');
			}
			
			if (($current_page < $count_pages - 2) &&
				($count_pages > 5)
			) {
				//добавляю ссылку на последнюю страницу
				$sr['class'] = '';
				$sr['number'] = $count_pages;
				$sr['link'] = $link . '/' . $count_pages;
				$pages .= '... ' . $this->getReplaceTemplate($sr, 'pagination_number');
			}
			
			return $pages;
		} else {
			return '';
		}
	}
	
	private function getOfferOrDescription()
	{
		if (isset($this->data['type'])) {
			$sr['link'] = $this->data['type'];
			//print_r($this->info);
			$sr['device_type'] = mb_strtolower($this->info['SEO']['sec_plural']);
			return $this->getReplaceTemplate($sr, 'section_offer');
		} else {
			return $this->getTemplate('section_frontpage_description');
		}
	}
}
