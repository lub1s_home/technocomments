<?php
require_once 'global_class.php';

class Question extends GlobalClass {

	public function __construct($db) 
	{
		parent::__construct('questions', $db);
	}
	
	public function getQuestionList($section_id) 
	{
		return $this->getAllWithTheFieldsAndWhere(array('id', 'question'), 'section_id = '.$section_id);
	}
}
?>