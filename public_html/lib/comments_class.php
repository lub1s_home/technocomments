<?php
	require_once("global_class.php");
	
	class Comments extends GlobalClass 
	{
	
		public function __construct($db) {
			parent::__construct('comments', $db);
		}
		
		// эта функция вставляет запись и возвращает Id вставленной записи
		public function addComment($device_id, $comment, $name) 
		{
			$result = $this->insertAndGetId(array('device_id' => $device_id, 'comment' => $comment, 'name' => $name, 'date' => time()));
			return $result;
		}

		public function getAllModeratedComments($device_id) 
		{
			return $this->getAllWithWhereAndOrder('device_id = '.$device_id.' AND moderation = 1', 'date', false);
		}
		
		public function getAllUnmoderatedComments() 
		{
			return $this->getAllWithWhere('moderation = 0');
		}
		
		// если одобряю комментарий гостя, то надо указать "name" и "lastname"
		// если пользователя - то не передавать "name" и "lastname"
		public function admApproveComment($id, $comment, $name)
		{
			return $this->editString(array('moderation' => 1 , 'comment' => $comment, 'name' => $name), 'id', $id);
		}
		
		// Сортировка должна быть по дате. Хотя ни чем не будет отличаться от сортировки по id. 
		// Поэтому сортировка по id, т.к. по этому полю есть уникальный индекс в БД
		public function getAllModeratedCommentsSortDate() 
		{
			return $this->getAllOnField('moderation', 1, 'id', false);
		}
		
		public function getSmallInfoForSection($where) 
		{
			return $this->getAllWithTheFieldsAndWhere(array('id', 'device_id'), '(moderation = 1)'.$where);
		}

	}
?>