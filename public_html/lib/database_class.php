<?php

require_once 'config_class.php';
require_once 'checkvalid_class.php';

class DataBase 
{
	private $config;
	private $mysqli; //идентификатор соединения
	private $valid;
	
	private static $database = null;
	
	private function __construct() 
	{
		$this->config = new Config();
		$this->valid = new CheckValid();
		$this->mysqli = new mysqli($this->config->host, $this->config->user, $this->config->password, $this->config->db);
		$this->mysqli->query("SET NAMES 'utf8'");
	}
	
	public static function getObject() 
	{
		if (self::$database === null) self::$database = new DataBase();
		return self::$database;
	}
	
	private function query($query) 
	{
		return $this->mysqli->query($query);
	}
	
	private function select($table_name, $fields, $where = '', $order = '', $up = true, $limit = '', $distinct = false) 
	{
		for ($i = 0; $i < count($fields); $i++)
		{
			if ((strpos($fields[$i], '(') === false) && ($fields[$i] != '*')) $fields[$i] = '`'.$fields[$i].'`';
		}
		$fields = implode(',', $fields); //превращаю массив $fields в строку
		$table_name = $this->config->db_prefix.$table_name;
		if (!$order) $order = 'ORDER BY `id`';
		else
		{
			if ($order != 'RAND()')
			{
				$order = 'ORDER BY `'.$order.'`';
				if (!$up) $order .= ' DESC';
			}
			else $order = 'ORDER BY '.$order;
		}
		$dis = ($distinct === true)? 'DISTINCT ' : '';
		if ($limit) $limit = 'LIMIT '.$limit;
		if ($where) $query = 'SELECT '.$dis.$fields.' FROM '.$table_name.' WHERE '.$where.' '.$order.' '.$limit;
		else $query = 'SELECT '.$dis.$fields.' FROM '.$table_name.' '.$order.' '.$limit;
			//echo $query.'<br />';
		$result_set = $this->query($query);
		if (!$result_set) return false;
		$i = 0;
		$data = array();
		while ($row = $result_set->fetch_assoc())
		{
			$data[$i] = $row; //сформировал двумерный массив
			$i++;
		}
		$result_set->close();
		//print_r($data);
		return $data; //возвращаю двумерный массив
	}
	
	public function insert($table_name, $new_values) 
	{
		$table_name = $this->config->db_prefix.$table_name;
		$query = 'INSERT INTO '.$table_name.' (';
		foreach ($new_values as $field => $value) $query .= '`'.$field.'`,';
		$query = substr($query, 0, -1);
		$query .=') VALUES (';
		foreach ($new_values as $value) $query .= "'".addslashes($value)."',";
		$query = substr($query, 0, -1);
		$query .= ')';
		return $this->query($query);
	}
	
	public function insertAndGetId($table_name, $new_values) 
	{
		$result = $this->insert($table_name, $new_values);
		if ($result) return $this->mysqli->insert_id;
		else return false;
	}
	
	public function insetFewStrings($table_name, $arr_fields, $arr_new_values) 
	{
		$table_name = $this->config->db_prefix.$table_name;
		$query = 'INSERT INTO '.$table_name.' (';
		
		for ($i = 0; $i < count($arr_fields); $i++) {
			$query .= '`'.$arr_fields[$i].'`,';
		}
		$query = substr($query, 0, -1);
		$query .=') VALUES (';
		
		for ($i = 0; $i < count($arr_new_values); $i++) {
			for ($j = 0; $j < count($arr_new_values[$i]); $j++) {
				$query .= "'".addslashes($arr_new_values[$i][$j])."',";
			}
			$query = substr($query, 0, -1);
			$query .= '), (';
		}
		$query = substr($query, 0, -3);
		return $this->query($query);
	}
	
	private function update($table_name, $upd_fields, $where) 
	{
		$table_name = $this->config->db_prefix.$table_name;
		$query = 'UPDATE '.$table_name.' SET ';
		foreach ($upd_fields as $field => $value) $query .="`$field` = '".addslashes($value)."',";
		$query = substr($query, 0, -1);
		if ($where) {
			$query .= ' WHERE '.$where;
			return $this->query($query);
		}
		else return false;
	}
	
	public function delete($table_name, $where = '') 
	{
		$table_name = $this->config->db_prefix.$table_name;
		if ($where) {
			$query = 'DELETE FROM '.$table_name.' WHERE '.$where;
			return $this->query($query);
		}
		return false;
	}
	
	public function deleteAll($table_name) 
	{
		$table_name = $this->config->db_prefix.$table_name;
		$query = 'TRANCATE TABLE `'.$table_name.'`';
		return $this->query($query);
	}
	
	//получить значение какого-либо поля таблицы "field_out", где значение другого поля "field_in" = "value_in"
	public function getField($table_name, $field_out, $field_in, $value_in) 
	{ 
		$data = $this->select($table_name, array($field_out), "`$field_in`='".addslashes($value_in)."'");
		if (count($data) != 1) return false;
		//если select вернул 0 или более 1 строки, то использовать данный метод некорректно
		return $data[0][$field_out];
		//возвращаю значение искомого поля
	}
	
	public function getFieldOnID($table_name, $id, $field_out) 
	{
		if (!$this->existsID($table_name, $id)) return false;
		return $this->getField($table_name, $field_out, 'id', $id);
	}
	
	public function getAll($table_name, $order, $up) 
	{
		return $this->select($table_name, array('*'), '', $order, $up);
	}
	
	
	public function getAllOnField($table_name, $field, $value, $order, $up) 
	{
		return $this->select($table_name, array('*'), "`$field`='".addslashes($value)."'", $order, $up);
	}
	
	// копия метода из ДЗ 5.2.
	public function getLastID($table_name) 
	{
		$data = $this->select($table_name, array('MAX(`id`)'));
		return $data[0]['MAX(`id`)'];
	}
	
	public function deleteOnID($table_name, $id) 
	{
		if (!$this->existsID($table_name, $id)) return false;
		return $this->delete($table_name, "`id` = '$id'");
	}
	
	public function setField($table_name, $field, $value, $field_in, $value_in) 
	{
		return $this->update($table_name, array($field => $value), "`$field_in` = '".addslashes($value_in)."'");
	}
	
	public function editString($table_name, $upd_fields, $field_in, $value_in) 
	{
		return $this->update($table_name, $upd_fields, "`$field_in` = '".addslashes($value_in)."'");
	}
	
	public function updateOnID($table_name, $upd_fields, $id) 
	{
		return $this->update($table_name, $upd_fields, '`id` = '.$id);
	}
	
	public function setFieldOnID($table_name, $id, $field, $value) 
	{
		if (!$this->existsID($table_name, $id)) return false;
		return $this->setField($table_name, $field, $value, 'id', $id);
	}
	
	public function getElementOnID($table_name, $id) 
	{
		if (!$this->existsID($table_name, $id)) return false;
		$arr = $this->select($table_name, array('*'), "`id` = '$id'");
		return $arr[0];
	}
	
	public function getElementOnField($table_name, $field_name, $field_value) 
	{
		$element = $this->select($table_name, array('*'), "`$field_name` = '$field_value'");
		if (count($element) != 1) return false;
		return $element[0];
	}
	
	public function getRandomElements($table_name, $count) 
	{
		return $this->select($table_name, array('*'), '', 'RAND()', true, $count);
	}
	
	public function getCount($table_name, $where) 
	{
		$data = $this->select($table_name, array('COUNT(`id`)'), $where);
		return $data[0]['COUNT(`id`)'];
	}
	
	// 5.2 домашнее задание
	// метод, позволяющий узнать последний ID в заданной таблице
	public function getMaxID($table_name) 
	{
		$data = $this->select($table_name, array('MAX(`id`)'));
		return $data[0]['MAX(`id`)'];
	}
	
	// 5.3. домашнее задание
	// метод, позволяющий узнать максимальное значение у заданного поля в заданной таблице
	public function getMaxValueInField($table_name, $field) 
	{
		$data = $this->select($table_name, array('MAX(`$field`)'));
		if (count($data) === 0) return false;
		return $data[0][$field];
	}
	
	// 5.4. домашнее задание
	// метод, позволяющий узнать минимальное значение у заданного поля в заданной таблице
	public function getMinValueInField($table_name, $field) 
	{
		$data = $this->select($table_name, array('MIN(`$field`)'));
		if (count($data) === 0) return false;
		return $data[0][$field];
	}
	
	// 5.5. метод, позволяющий вытащить все записи из таблицы, значения поля которых находятся в заданном интервале
	public function getIntervalValue($table_name, $field, $min, $max) 
	{
		$where = $field.' < '.$max.' AND '.$field.' > '.$min;
		return $this->select($table_name, array($field), $where);
	}
	
	
	public function isExists($table_name, $field, $value) 
	{
		$data = $this->select($table_name, array('id'), "`$field` = '".addslashes($value)."'");
		if (count($data) === 0) return false;
		return true;
	}
	
	private function existsID($table_name, $id) 
	{
		if (!$this->valid->validID($id)) return false;
		$data = $this->select($table_name, array('id'), "`id`='".addslashes($id)."'");
		if (count($data) === 0) return false;
		return true;
	}
	
	public function search($table_name, $words, $fields) 
	{
		if ($words == '') return false;
		$where = '';
		$arraywords = explode(' ', $words);
		$logic = ' OR ';
		
		foreach($arraywords as $key => $value) {
			if (isset($arraywords[$key - 1])) $where .= $logic;
			for ($i = 0; $i < count($fields); $i++) {
				$where .= "`".$fields[$i]."` LIKE '%".addslashes($value)."%'";
				if (($i + 1) != count($fields)) $where .= " OR ";
			}
		}
		$results = $this->select($table_name, array('*'), $where);
		if (!$results) return false;
		$k = 0;
		$data = array();
		for ($i = 0; $i < count($results); $i++) {
			for ($j = 0; $j < count($fields); $j++) {
				$results[$i][$fields[$j]] = mb_strtolower(strip_tags($results[$i][$fields[$j]]));
			}
			$data[$k] = $results[$i];
			$data[$k]['relevant'] = $this->getRelevantForSearch($results[$i], $fields, $words);
			$k++;
		}
		$data = $this->orderResultSearch($data, 'relevant');
		return $data;
	}
	
	private function getRelevantForSearch($result, $fields, $words) 
	{
		$relevant = 0;
		$arraywords = explode(' ', $words);
		for ($i = 0; $i < count($fields); $i++) {
			for ($j = 0; $j < count($arraywords); $j++) {
				if ($arraywords[$j] != '')
					$relevant += substr_count($result[$fields[$i]], $arraywords[$j]);
			}
		}
		return $relevant;
	}
	
	private function orderResultSearch($data, $order) 
	{
		for ($i = 0; $i < count($data) - 1; $i++) {
			$k = $i;
			for ($j = $i + 1; $j < count($data); $j++) {
				if ($data[$j][$order] > $data[$k][$order]) $k = $j;
			}
			$temp = $data[$k];
			$data[$k] = $data[$i];
			$data[$i] = $temp;
		}
		return $data;
	}
	
	public function getAllWithWhere($table_name, $where) 
	{
		return $this->select($table_name, array('*'), $where);
	}
	
	public function getDistinctWithTheFieldsAndWhere($table_name, $fields, $where) 
	{
		return $this->select($table_name, $fields, $where, '', true, '', true);
	}
	
	public function getAllWithWhereAndOrder($table_name, $where, $order, $up) 
	{
		return $this->select($table_name, array('*'), $where, $order, $up);
	}
	
	public function getFieldsWithOrder($table_name, $fields, $order, $up)
	{
		return $this->select($table_name, $fields, $where = '', $order, $up);
	}
	
	public function getAllWithTheFields($table_name, $fields) 
	{
		return $this->select($table_name, $fields);
	}
	
	public function getAllWithTheFieldsAndWhere($table_name, $fields, $where) 
	{
		return $this->select($table_name, $fields, $where);
	}
	
	public function getFewFreshString($table_name, $where, $limit) 
	{
		return $this->select($table_name, array('*'), $where, 'release_date', false, $limit);
	}
	
	public function getFields($table_name, $fields)
	{
		return $this->select($table_name, $fields);
	}
	
	public function __destruct() 
	{
		if ($this->mysqli) $this->mysqli->close();
	}
	
}
?>