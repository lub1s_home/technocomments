<?php

require_once 'adm_modules_class.php';
require_once 'comments_class.php';
require_once 'rating_class.php';

require_once 'device_class.php';
require_once 'section_class.php';
require_once 'company_class.php';

class Adm_CommentsContent extends Adm_Modules 
{	
	private $comments_obj;
  	private $device_obj;
	private $section_obj;
	private $company_obj;
	
	public function __construct($db) {
		parent::__construct($db);
		
		$this->comments_obj = new Comments($db);
      	$this->device_obj = new Device($db);
		$this->section_obj = new Section($db);
		$this->company_obj = new Company($db);
		$this->rating_obj = new Rating($db);
	}
	
	protected function getTitle() 
	{
		return 'Все отзывы';
	}
	
	protected function getMiddle() 
	{
		// конкретный комментарий
		if (isset($this->data['id']))
		{
			$comments = $this->comments_obj->get($this->data['id']);
			//print_r($comments);
			$rating_arr = $this->rating_obj->getAllWithTheFieldsAndWhere(array('rating'), "`comment_id` = $comments[id]");
			$rating_str = '';
			for ($i = 0; $i < count($rating_arr); $i++)
			{
				$rating_str .= $rating_arr[$i]['rating'] . ' ';
			}
          	$device = $this->device_obj->get($comments['device_id']);
			$section = $this->section_obj->get($device['section_id']);
			$company = $this->company_obj->get($device['company_id']);
			
			$sr['device_link'] = $section['for_link'] . '/' . mb_strtolower(str_replace(' ', '_', $company['title'])) . '/' . mb_strtolower(str_replace(' ', '_', $device['device']));
			$sr['device_link_anchor'] = $section['name_singular'] . ' ' . $company['title'] . ' ' . $device['device'];
			
			$sr['name'] = $comments['name'];
			
			$sr['comment'] = $comments['comment'];
			$sr['info'] = 'id: ' . $comments['id'] . ' / оценка: ' . $rating_str;
			$sr['comment_id'] = $comments['id'];
			$sr['device_id'] = $comments['device_id'];
			
			$snip = $this->getReplaceTemplate($sr, 'adm_comments_moderation_snippets');
			$sr_page['page_name'] = 'Редактировать отзыв';
			return $this->getReplaceTemplate($sr_page, 'adm_page_name') . $snip;
		}
		// весь список одобренных комментариев
		else 
		{
			$all_comm = $this->comments_obj->getAllModeratedCommentsSortDate();
			$snips = '';
			for ($i = 0; $i < count($all_comm); $i++) 
			{
				$sr_snip['link'] = '?view=comments&amp;id='.$all_comm[$i]['id'];
				$sr_snip['title'] = $this->formatDate($all_comm[$i]['date']);
				
				$snips .= $this->getReplaceTemplate($sr_snip, 'adm_comments_all_snippet');
			}
            $sr_page['page_name'] = 'Одобренные отзывы &ndash; ' . count($all_comm) . ' шт';
			return $this->getReplaceTemplate($sr_page, 'adm_page_name') . $snips;
		}
	}

}

?>