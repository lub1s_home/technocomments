<?php

require_once 'global_class.php';

class Rating extends GlobalClass {

	public function __construct($db) {
		parent::__construct('rating', $db);
	}
	
	public function addRating($arr_new_values) {
		return $this->insetFewStrings(array('comment_id', 'question_id', 'rating'), $arr_new_values);
	}
	
	// $where - строка, последовательность comment_id, например "4,5,8"
	public function getInfoForCountingRating($where) 
	{	
		$rating = $this->getAllWithTheFieldsAndWhere(array('question_id', 'rating'), '`comment_id` IN ('.$where.')');
		
		for ($i = 0; $i < count($rating); $i++)
		{
			if (!isset($arr[ $rating[$i]['question_id'] ]))
				$arr[ $rating[$i]['question_id'] ] = 0;
			$arr[ $rating[$i]['question_id'] ] += $rating[$i]['rating'];
		}
		
		foreach ($arr as $key => $value)
		{
			$arr[$key] /= (count($rating) / count($arr));
		}
		return $arr;
	}
	
	// $where - строка, последовательность comment_id, например "4,5,8"
	public function getCountRatingOnDevices($where, $arr_commentId_gBy_deviceId) {
		
		$all_rating = $this->getAllWithTheFieldsAndWhere(array('comment_id', 'rating'), '`comment_id` IN ('.$where.')');
		
		for ($i = 0; $i < count($all_rating); $i++) {
			$rating_gBy_commentId[ $all_rating[$i]['comment_id'] ][] = $all_rating[$i]['rating'];
		}
		
		foreach ($rating_gBy_commentId as $key => $value) {
			$summ = 0;
			for ($i = 0; $i < count($value); $i++) {
				$summ += $value[$i];
			}
			$rating_average_gBy_commentId[$key] = $summ / count($value);
		}
		
		foreach ($arr_commentId_gBy_deviceId as $key => $value) {
			$summ = 0;
			$count = 0;
			for ($i = 0; $i < count($value); $i++) {
				if (isset($rating_average_gBy_commentId[ $value[$i] ])) {
					$summ += $rating_average_gBy_commentId[ $value[$i] ];
					$count++;
				}
			}
			$count_rating_on_devices[$key] = $summ / $count;
		}
		return $count_rating_on_devices;
	}
}
?>