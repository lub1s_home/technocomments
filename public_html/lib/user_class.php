<?php
	require_once 'global_class.php';
	
	class User extends GlobalClass {
	
		public function __construct($db) {
			parent::__construct('users', $db);
		}
		
		public function addUser($name, $lastname, $email, $password, $regDate) {
			if (!$this->checkValid($password, $regDate)) return false;
			return $this->add(array('name' => $name, 'lastname' => $lastname, 'email' => $email, 'password' => $password, 'regDate' => $regDate, 'confirm_email' => 0));
		}
		
		public function isExistsUser($email) {
			return $this->isExists('email', $email);
		}
		
		// получает таблицу нужных пользователей с полями 'id', 'name', 'lastname'
		public function getSmallInfoForComments($where) {
			return $this->getAllWithTheFieldsAndWhere(array('id', 'name', 'lastname'), $where);
		}
		
		public function checkUserAndGet($email, $password) {
			$user = $this->getUserOnEmail($email);
			if (!$user) return false;
			if ($user['password'] !== $password) return false;
			return $user;
		}
		
		public function getUserOnEmail($email) {
			return $this->getElementOnField('email', $email);
			// на выходе одномерный массив (вся информация о пользователе)
		}
		
		public function getIdOnEmail($email) {
			return $this->getField('id', 'email', $email);
		}
		
		public function getLoginOnId($id) {
			return $this->getField('login', 'id', $id);
		}
		
		private function checkValid($password, $regDate) {
			if (!$this->valid->validHash($password)) return false;
			if (!$this->valid->validTimeStamp($regDate)) return false;
			return true;
		}
		
		public function confirmEmail($user_id) {
			$this->updateOnID($user_id, array('confirm_email' => 1));
		}
	}

?>