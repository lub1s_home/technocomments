<?php

require_once 'config_class.php';
require_once 'messageemail_class.php';

class Mail {

	private $config;
	private $message_email; //объект с сообщениями на почту
	
	public function __construct() {
		$this->config = new Config;
		$this->message_email = new MessageEmail;
	}
	
	//отправка письма с подтверждением почты
	public function sendMailForConfirmEmail($mail_to, $hash_password) {
		$subject = $this->message_email->getTitle('EMAIL_CONFIRM');
		$message = $this->message_email->getText('EMAIL_CONFIRM').' '.$this->config->address.'/functions.php?email='.$mail_to.'&confirm_email='.$hash_password;
		$this->sendMail($mail_to, $subject, $message);
	}
	
	// отправка письма с новым девайсом и отзывом на мою почту
	public function sendMailOffer($section, $device, $name, $comment, $rating)
	{
		$subject = 'Новый отзыв TC';
		$message = 
			'<b>Тип</b>            <br />' . $section              . '<hr />' .
			'<b>Устройство</b>     <br />' . $device               . '<hr />' .
			'<b>Имя</b>            <br />' . $name                 . '<hr />' .
			'<b>Отзыв</b>          <br />' . nl2br($comment, true) . '<hr />' .
			'<b>Оценка</b>         <br />' . $rating               . '<hr />' .
			'<b>Печать времени</b> <br />' . time();
		$mail_to = $this->config->adm_email;
		$this->sendMailHTML($mail_to, $subject, $message);
	}
	
	private function sendMail($mail_to, $subject, $message) {
		$mail_from = 'admin@technocomments.info';
		$subject = "=?utf-8?B?".base64_encode($subject)."?=";
		$headers = "From: $mail_from\r\nReply-to: $mail_from\r\nContent-type: text/plain; charset=utf-8\r\n";
		mail($mail_to, $subject, $message, $headers);
	}	
	
	private function sendMailHTML($mail_to, $subject, $message) {
		$mail_from = 'admin@technocomments.info';
		$subject = "=?utf-8?B?".base64_encode($subject)."?=";
		$headers = "From: $mail_from\r\nReply-to: $mail_from\r\nContent-type: text/html; charset=utf-8\r\n";
		mail($mail_to, $subject, $message, $headers);
	}
}

?>