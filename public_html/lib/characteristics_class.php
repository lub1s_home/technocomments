<?php
	require_once 'global_class.php';
	
	class Characteristics extends GlobalClass {
	
		public function __construct($db) {
			parent::__construct('characteristics', $db);
		}
		
		public function setName($id, $new_name) {
			return $this->setFieldOnID($id, 'name', $new_name);
		}
		
		public function setValue($id, $new_value) {
			return $this->setFieldOnID($id, 'value', $new_value);
		}
		
		public function addNewCharacteristic($device_id, $name, $value) {
			return $this->add(array('device_id' => $device_id , 'name' => $name, 'value' => $value));
		}
		
		public function editCharacteristic($id, $name, $value)
		{
			return $this->updateOnID($id, array('name' => $name, 'value' => $value));
		}
	}
?>