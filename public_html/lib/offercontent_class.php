<?php

require_once 'modules_class.php';
require_once 'question_class.php';
require_once 'rating_class.php';
require_once 'section_class.php';

class OfferContent extends Modules 
{
	private $info;
	private $question;
	private $section;
	//private $notfound = false; // если ли на странице ошибка 404?
	
	public function __construct($db)
	{
		parent::__construct($db);
		
		$this->section = new Section($db);
		$this->question = new Question($db);
		
		$section = $this->modules_info['sec_ar'];
		
		for ($i = 0; $i < count($section); $i++)
		{
			if ( $this->data['section'] == $section[$i]['for_link'] ) 
			{
				$this->info['section'] = $section[$i];
				break;
			}
			if ( $i + 1 == count($section) )
			{
				header('HTTP/1.0 404 Not Found');
				$this->notfound = true;
				return;
			}
		}
		// список вопросов об этом устройстве
		$this->info['question_list'] = $this->question->getQuestionList($this->info['section']['id']);
		//print_r($this->info);
	}
	
	protected function getTitle() 
	{
		if ($this->notfound === false) 
			return $this->info['section']['name_singular'] . ' - оставить отзыв';
		else 
			return 'Страница не найдена - 404';
	}
	
	protected function getDescription() 
	{
		if ($this->notfound === false) 
			return $this->info['section']['name_singular'] . ' - оставить отзыв';
		else 
			return 'Страница не найдена - 404';
	}
	
	protected function getKeyWords() 
	{
		if ($this->notfound === false) 
			return $this->info['section']['name_singular'] . ' - оставить отзыв';
		else 
			return 'Страница не найдена - 404';
	}
	
	protected function getCanonical() 
	{
		if ($this->notfound === false) 
			return '/' . $this->data['section'] . '/offer';
		else 
			return '/notfound';
	}
	
	protected function getMiddle() 
	{
		if ($this->notfound === false) 
		{
			$section_arr = $this->modules_info['sec_ar'];
			
			$sr['name_singular'] = $this->info['section']['name_singular'];
			$sr['name_plural'] = $this->info['section']['name_plural'];
			$sr['section_link'] = $this->info['section']['for_link'];
			
			for ($i = 0; $i < count($section_arr); $i++)
			{
				$sr_option['option'] = $section_arr[$i]['name_singular'];
				$sr_option['value'] = '/' . $section_arr[$i]['for_link'];
				$sr_option['selected'] = ($section_arr[$i]['for_link'] == $this->data['section']) ? 'selected' : '';
				
				$sr['options'] .= $this->getReplaceTemplate($sr_option, 'option');
			}
			
			$sr['device'] = $_SESSION['device_for_offer'];
			$sr['name'] = $_SESSION['name_for_offer'];
			$sr['comment'] = $_SESSION['comment_for_offer'];
			
			$sr['message'] = '';
			$message = $this->getMessage();
			if ($message)
			{
				$sr_message['message'] = $message;
				$sr['message'] = $this->getReplaceTemplate($sr_message, 'message');
			}
			
			$sr['questions'] = $this->getQuestions();
			$sr['count_questions'] = count( $this->info['question_list'] );
			
			return $this->getReplaceTemplate($sr, 'offer');
		}
		else
			return $this->getTemplate('not_found');
	}
	
	private function getQuestions() 
	{
		$title = array('отлично', 'хорошо', 'средне', 'плохо', 'ужасно');
		
		for ($i = 0; $i < count($this->info['question_list']); $i++) {
			
			$sr['question_id'] = $this->info['question_list'][$i]['id'];
			$sr['question'] = $this->info['question_list'][$i]['question'];
			$sr['number'] = $i;
			
			// проверяю, отвечал ли пользователь на текущий вопрос
			$variant = isset( $_SESSION['rating_'.$i] ) ? 
				$_SESSION['rating_'.$i] : false;
			
			$rating_items = '';
			for ($j = 0; $j < 5; $j++) {
				
				$deep_sr['check'] = ($variant == 5 - $j)? 'checked' : '';
				$deep_sr['item_num'] = 5 - $j;
				$deep_sr['number'] = $i;
				$deep_sr['title'] = $title[$j];
				$rating_items .= $this->getReplaceTemplate($deep_sr, 'rating_item');
			}
			$sr['rating_items'] = $rating_items;
			
			$quest .= $this->getReplaceTemplate($sr, 'rating');
		}
		return $quest;
	}
}
?>