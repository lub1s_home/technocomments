<?php
	require_once 'config_class.php';
	
	abstract class GlobalMessage {
		
		private $data; //массив отпарсенного ini-файла
		
		// $prefix чтобы работали сообщения из админки
		
		public function __construct($file, $bool = false) {
			$config = new Config();
			$prefix = ($bool == false)? '' : '../';
			$this->data = parse_ini_file($prefix.$config->dir_text.$file.'.ini');
		}
		
		public function getTitle($name) {
			return $this->data[$name.'_TITLE'];
		}
		
		public function getText($name) {
			return $this->data[$name.'_TEXT'];
		}
	}
?>