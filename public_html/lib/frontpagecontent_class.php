<?php
	require_once 'modules_class.php';
	
	class FrontPageContent extends Modules {
		
		public function __construct($db) {
			parent::__construct($db);
		} 
		
		protected function getTitle() {
			return 'Отзывы о компьютерной технике';
		}
		
		protected function getDescription() {
			return 'Пишите и читайте отзывы о компьютерной технике';
		}
		
		protected function getKeyWords() {
			return 'отзывы, компьютерная техника';
		}
		
		protected function getMiddle() {
			return $this->getTemplate('front_page');
		}

	}
?>