<?php

require_once 'modules_class.php';

class NotFoundContent extends Modules {
	
	public function __construct($db) {
		parent::__construct($db);
		
		header('HTTP/1.0 404 Not Found');
	}
	
	protected function getTitle() {
		return 'Страница не найдена - ошибка 404';
	}
	
	protected function getDescription() {
		return 'Страница не найдена - ошибка 404';
	}
	
	protected function getKeyWords() {
		return 'Страница не найдена - ошибка 404';
	}
	
	protected function getMiddle() {
		return $this->getTemplate('not_found');
	}
	
	protected function getCanonical(){
    	return '/notfound';
	}
	
}

?>