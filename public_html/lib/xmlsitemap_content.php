<?php

require_once 'config_class.php';
require_once 'company_class.php';
require_once 'device_class.php';
require_once 'section_class.php';

class XMLSiteMapContent 
{
	private $config;
	private $data;
	
	private $company;
	private $device;
	private $section;
	
	public function __construct($db) 
	{
		session_start();
		$this->config = new Config();
		$this->company = new Company($db);
		$this->device = new Device($db);
		$this->section = new Section($db);
		$this->data = $this->secureData($_GET);
	}
	
	public function getContent() 
	{
		$section = $this->section->getAllLinks();
		$company = $this->company->getTitlesList();
		$device = $this->device->getAll();
		
		//дополнительный массив, чтобы не было ошибки при нарушенной последовательности id-шников в таб. "Sections"
		$arr_sec = array();
		for ($i = 0; $i < count($section); $i++) 
		{
			$arr_sec[ $section[$i]['id'] ] = $section[$i]['for_link'];
		}
		
		//дополнительный массив, чтобы не было ошибки при нарушенной последовательности id-шников в таб. "Company"
		$arr_com = array();
		for ($i = 0; $i < count($company); $i++) 
		{
			$arr_com[ $company[$i]['id'] ] = $this->replaceSpaces($company[$i]['title']);
		}
		
		for ($i = 0; $i < count($device); $i++) 
		{
			$sr_link['link'] = $arr_sec[ $device[$i]['section_id'] ].'/'.$arr_com[ $device[$i]['company_id'] ].'/'.$this->replaceSpaces($device[$i]['device']);
			$sr_link['lastmod'] = date('c', $device[$i]['lastmod']);
			$links .= $this->getReplaceTemplate($sr_link, 'xml_sitemap_url');
			
		}
		
		$sr['urls'] = $links;
		
		header('Content-type: text/xml');
		return $this->getReplaceTemplate($sr, 'xml_sitemap_main');
	}
	
	private function getTemplate($name) 
	{
		$text = file_get_contents($this->config->dir_tmpl.$name.'.tpl');
		return str_replace("%address%", $this->config->address, $text);
	}
	
	private function getReplaceTemplate($sr, $template) 
	{
		return $this->getReplaceContent($sr, $this->getTemplate($template));
	}
	
	private function getReplaceContent($sr, $content) 
	{
		$search = array();  // элементы, которые хотим найти
		$replace = array(); // элементы, на которые хотим заменить
		$i = 0;
		foreach ($sr as $key => $value) {
			$search[$i] = "%$key%";
			$replace[$i] = $value;
			$i++;
		}
		return str_replace($search, $replace, $content);
	}
	
	private function secureData($data) 
	{
		foreach($data as $key => $value) {
			if (is_array($value)) $this->secureData($value);
			else $data[$key] = htmlspecialchars($value);
		}
		return $data;
	}
	
	private function replaceSpaces($string) 
	{
		return mb_strtolower(str_replace(' ', '_', $string));
	}

}
?>