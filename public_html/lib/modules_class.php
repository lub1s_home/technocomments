<?php
require_once 'config_class.php';
require_once 'checkvalid_class.php';
require_once 'message_class.php';

require_once 'device_class.php';
require_once 'company_class.php';
require_once 'section_class.php';


abstract class Modules 
{
	protected $data;
	protected $notfound = false;
	
	protected $config;
	protected $valid;
	protected $message;
	
	protected $sec_obj;
	protected $dev_obj;
	protected $com_obj;
	
	protected $modules_info;
	
	// $db - объект базы данных
	public function __construct($db) 
	{
		session_start();
		$this->data = $this->secureData($_GET);
		
		$this->config = new Config();
		$this->valid = new CheckValid();
		$this->message = new Message();
		
		$this->sec_obj = new Section($db);
		$this->dev_obj = new Device($db);
		$this->com_obj = new Company($db);
		
		$this->modules_info['sec_ar'] = $this->sec_obj->getGlobalMenu();
		
		if (isset($this->data['type']))
		{
			for ($i = 0; $i < count($this->modules_info['sec_ar']); $i++)
			{
				if ($this->data['type'] == $this->modules_info['sec_ar'][$i]['for_link'])
				{
					$this->modules_info['sec_id'] = $this->modules_info['sec_ar'][$i]['id'];
					$this->modules_info['iterator_for_sec_ar'] = $i;
					break;
				}
			}
			if (!$this->modules_info['sec_id'])
			{
				header('HTTP/1.0 404 Not Found');
				$this->notfound = true;
			}
		}
		
		$this->modules_info['com_ar'] = $this->getCompanyArrForSection();
		//print_r($this->modules_info);
	}
	
	public function getContent() 
	{
		$sr['title'] = $this->getTitle();
		$sr['meta_desc'] = $this->getDescription();
		$sr['meta_keys'] = $this->getKeyWords();
		$sr['canonical'] = $this->getCanonical();
		
		$sr['menu_brands_button'] = $this->getBrandsMenuButton();
		$sr['menu_brands'] = $this->getBrandsMenu();
		$sr['menu_sections_items'] = $this->getSectionsMenu();
		
		$sr['middle'] = $this->getMiddle();
		$sr['bottom'] = $this->getBottom();
		$sr['site_name'] = $this->config->sitename;
		
		return $this->getReplaceTemplate($sr, 'main');
	}
	
	protected function getSectionsMenu()
	{
		//меню с сортировкой по wordstat'у
		$menu = $this->modules_info['sec_ar'];
		$sr['dir'] = 'sections';
		$text = '';
		for ($i = 0; $i < count($menu); $i++)
		{
			$sr['src'] = $menu[$i]['for_link'];
			$sr['title'] = $menu[$i]['name_plural'];
			$sr['link'] = $menu[$i]['for_link'];
			$sr['class'] = ((isset($this->data['type'])) && ($this->data['type'] == $sr['link'])) ?
				'class=\'active\'' : 
				'';
			$text .= $this->getReplaceTemplate($sr, 'main_menu_item');
		}
		return $text;
	}
	
	protected function getBrandsMenu()
	{
		//показывать бренды, если это не главная страница, и она существует
		if ((isset($this->data['type'])) && (!$this->notfound))
		{
			$sr_deep['dir'] = 'brands';
			$text = '';
			for ($i = 0; $i < count($this->modules_info['com_ar']); $i++)
			{
				$m_title = mb_strtolower(str_replace(' ', '_', $this->modules_info['com_ar'][$i]['title']));
				
				$sr_deep['src'] = $m_title;
				$sr_deep['title'] = $this->modules_info['com_ar'][$i]['title'];
				$sr_deep['link'] = $this->data['type'] . '/' . $m_title;
				$sr_deep['class'] = ((isset($this->data['filter'])) && ($this->data['filter'] == $m_title)) ?
					'class=\'active\'' : 
					'';
				$text .= $this->getReplaceTemplate($sr_deep, 'main_menu_item');
			}
			$sr['menu_brands'] = $text;
			return  $this->getReplaceTemplate($sr, 'main_menu_brands');
		}
		return '';
	}
	
	protected function getBrandsMenuButton()
	{
		//показывать бренды, если это не главная страница, и она существует
		if ((isset($this->data['type'])) && (!$this->notfound))
			return $this->getTemplate('main_menu_brands_button');
		else
			return '';
	}
	
	protected function getCanonical() 
	{
		return '';
	}
	
	abstract protected function getTitle();
	abstract protected function getDescription();
	abstract protected function getKeyWords();
	abstract protected function getMiddle();
	
	protected function getTop() 
	{
		return '';
	}
	
	protected function getBottom() 
	{
		return '';
	}
	
	private function secureData($data) 
	{
		foreach($data as $key => $value) 
		{
			if (is_array($value)) $this->secureData($value);
			else $data[$key] = htmlspecialchars($value);
		}
		return $data;
	}
	
	protected function formatDate($time) 
	{
		$months = array('Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря');
		$i = date('n', $time) - 1;
		return date('d', $time).' '.$months[$i].' '.date('Y - H:i', $time);
	}
	
	protected function getMessage($message = '') 
	{
		if ($message == '') 
		{
			if (!isset($_SESSION['message']))
				return '';
			
			$message = $_SESSION['message'];
			unset($_SESSION['message']);
		}
		return $this->message->getText($message);
	}
	
	protected function getTemplate($name) 
	{
		$text = file_get_contents($this->config->dir_tmpl.$name.'.tpl');
		return str_replace("%address%", $this->config->address, $text);
	}
	
	protected function getReplaceTemplate($sr, $template) 
	{
		return $this->getReplaceContent($sr, $this->getTemplate($template));
	}
	
	private function getReplaceContent($sr, $content) 
	{
		$search = array();  // элементы, которые хотим найти
		$replace = array(); // элементы, на которые хотим заменить
		$i = 0;
		foreach ($sr as $key => $value) 
		{
			$search[$i] = "%$key%";
			$replace[$i] = $value;
			$i++;
		}
		return str_replace($search, $replace, $content);
	}
	
	protected function redirect($link) 
	{
		header('Location: '.$link);
		exit;
	}
  
    protected function replaceSpaces($string, $by = '_') 
	{
		return mb_strtolower(str_replace(' ', $by, $string));
	}
	
	protected function getCompanyArrForSection()
	{
		if (isset($this->data['type']))
		{
			$where_for_getWhereForMenu = '`section_id` = '.$this->modules_info['sec_id'];
			$where_for_menu = $this->dev_obj->getWhereForMenu($where_for_getWhereForMenu);
			return $this->com_obj->getMenu($where_for_menu);
		}
		return $this->com_obj->getMenu();
	}
}
?>