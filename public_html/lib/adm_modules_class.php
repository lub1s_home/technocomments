<?php

require_once 'config_class.php';
require_once 'checkvalid_class.php';
require_once 'message_class.php';

abstract class Adm_Modules 
{
	protected $config;
	protected $valid;
	protected $data;
	
	// $db - объект базы данных
	public function __construct($db) 
	{
		session_start();
		$this->config = new Config();
		$this->data = $this->secureData($_GET);
		$this->message = new Message(true);
		$this->valid = new CheckValid();
	}
	
	public function getContent() 
	{
		if (isset($_SESSION['adm_auth']))
		{
			$sr['top'] = $this->getTemplate('adm_menu');
			$sr['middle'] = $this->getMiddle();
			$sr['title'] = $this->getTitle();
		}
		else
		{
			$sr['top'] = $this->getTemplate('adm_auth');
			$sr['middle'] = '';
			$sr['title'] = 'Авторизация';
		}
		return $this->getReplaceTemplate($sr, 'adm_main');
	}
	
	abstract protected function getTitle();
	abstract protected function getMiddle();
	
	private function secureData($data) 
	{
		foreach($data as $key => $value) 
		{
			if (is_array($value)) $this->secureData($value);
			else $data[$key] = htmlspecialchars($value);
		}
		return $data;
	}

	protected function formatDate($time) 
	{
		$months = array('Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря');
		$i = date('n', $time) - 1;
		return date('d', $time).' '.$months[$i].' '.date('Y - H:i', $time);
	}
	
	protected function getMessage($message_index) 
	{
		if ((isset($_SESSION['message_index'])) && ($_SESSION['message_index'] == $message_index))
		{
			$message = $_SESSION['message'];
			unset($_SESSION['message']);
			unset($_SESSION['message_index']); // определяет, в каком месте страницы вывести сообщение
			return $this->message->getText($message);
		}
		else return '';
		
	}		
	
	//надо переделать, чтобы каждый раз не парсился адрес
	protected function getTemplate($name) 
	{
		$text = file_get_contents('../'.$this->config->dir_tmpl.$name.'.tpl');
		$search = array('%address%', '%adm_directory%');
		$replace = array($this->config->address, $this->config->adm_directory);
		return str_replace($search, $replace, $text);
	}
	
	protected function getReplaceTemplate($sr, $template) 
	{
		return $this->getReplaceContent($sr, $this->getTemplate($template));
	}
	
	private function getReplaceContent($sr, $content) 
	{
		$search = array();  // элементы, которые хотим найти
		$replace = array(); // элементы, на которые хотим заменить
		$i = 0;
		foreach ($sr as $key => $value) 
		{
			$search[$i] = "%$key%";
			$replace[$i] = $value;
			$i++;
		}
		return str_replace($search, $replace, $content);
	}
	
	protected function redirect($link) 
	{
		header('Location: '.$link);
		exit;
	}
}
?>