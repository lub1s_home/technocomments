<?php

require_once 'characteristics_class.php';
require_once 'company_class.php';
require_once 'comments_class.php';
require_once 'device_class.php';
require_once 'modules_class.php';
require_once 'question_class.php';
require_once 'rating_class.php';
require_once 'section_class.php';
require_once 'video_class.php';

class DeviceContent extends Modules
{
	private $characteristic;
	private $comments;
	private $company;
	private $device;
	private $device_full_info;
	private $question;
	private $question_list;
	private $rating;
	private $section;
	private $session_for_current_device;
	private $video;
	
	public function __construct($db)
	{
		parent::__construct($db);
		
		$this->company = new Company($db);
		$this->comments = new Comments($db);
		$this->device  = new Device($db);
		$this->section = new Section($db);
		$this->question = new Question($db);
		$this->characteristic = new Characteristics($db);
		$this->rating = new Rating($db);
		$this->video = new Video($db);
		
		$this->device_full_info = $this->getDeviceFullInfo();
		
		if ($this->device_full_info) {
			$this->question_list = $this->question->getQuestionList($this->device_full_info['device_section']['id']);
			
			// пользователь пытался оставить коммент к текущему устройству?
			$this->session_for_current_device = ((isset($_SESSION['device_id_for_comment'])) &&
				($this->device_full_info['device_info']['id'] == $_SESSION['device_id_for_comment']))
				? true
				: false;
			
			// Last-Modified
			$lastmod = $this->device_full_info['device_info']['lastmod'];
			$last_modified = gmdate("D, d M Y H:i:s \G\M\T", $lastmod);
			$if_modified_since = false;
			if (isset($_ENV['HTTP_IF_MODIFIED_SINCE'])) {
				$if_modified_since = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));
			}
			if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
				$if_modified_since = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
			}
			if ($if_modified_since && $if_modified_since >= $lastmod) {
				header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
				exit;
			}
			header('Last-Modified: '. $last_modified);
		}
	}
	
	private function getDeviceFullInfo()
	{
		$dev_name = str_replace('_', ' ', $this->data['name']);
		$dev_type = $this->data['type'];
		$dev_company = str_replace('_', ' ', $this->data['company']);
		
		$device_info = $this->device->getAllOnField('device', $dev_name);
		if (!$device_info) {
			header('HTTP/1.0 404 Not Found');
			$this->notfound = true;
			return false;
		}
		
		// проверяю совпадения по компании и секции
		$true_string_number = false;

		for ($i = 0; $i < count($device_info); $i++) {
			$device_section_full_string = $this->section->get($device_info[$i]['section_id']);

			$device_company = $this->company->get($device_info[$i]['company_id']);
			$device_company_lower_first_letter = mb_strtolower($device_company['title']);
			
			// пробелы заменяются на тире, но в названии компании тоже может быть тире, поэтому такая проверка
			if (($device_section_full_string['for_link'] == $dev_type) &&
				($device_company_lower_first_letter == $dev_company)
			) {
				$true_string_number = $i;
				break;
			}
		}
		if ($true_string_number === false) {
			header('HTTP/1.0 404 Not Found');
			$this->notfound = true;
			return false;
		}
		
		$device_info = $device_info[$true_string_number];
		
		$device_full_info = array();
		$device_full_info['device_info'] = $device_info;
		$device_full_info['device_section'] = $device_section_full_string;
		$device_full_info['device_company'] = $device_company;
		
		$device_full_info['all_mod_comments'] = $this->comments->getAllModeratedComments(
			$device_full_info['device_info']['id']
		);
		
		//собираю рейтинг
		$where_for_rating = '';
		for ($i = 0; $i < count($device_full_info['all_mod_comments']); $i++) {
			$where_for_rating .= $device_full_info['all_mod_comments'][$i]['id'] . ',';
		}
		if (!$where_for_rating) {
			$device_full_info['counting_raiting'] = '';
		} else {
			$where_for_rating = substr($where_for_rating, 0, -1);
			//echo $where_for_rating;
			$device_full_info['counting_raiting'] = $this->rating->getInfoForCountingRating($where_for_rating);
			//print_r($device_full_info);
		}
		return $device_full_info;
	}
	
	protected function getTitle()
	{
		if ($this->notfound === false) {
			return 'Отзывы о ' .
				$this->device_full_info['device_section']['name_prepositional'] . ' ' .
				$this->device_full_info['device_company']['title'] . ' ' .
				$this->device_full_info['device_info']['device'];
		} else {
			return 'Страница не найдена - ошибка 404';
		}
	}
	
	protected function getDescription()
	{
		if ($this->notfound === false) {
			return 'Мнения владельцев о ' .
				$this->device_full_info['device_section']['name_prepositional'] . ' ' .
				$this->device_full_info['device_company']['title'] . ' ' .
				$this->device_full_info['device_info']['device'] .
				'. Реальные отзывы, оценки, характеристики, фото, видео.';
		} else {
			return 'Страница не найдена - ошибка 404';
		}
	}
	
	protected function getKeyWords()
	{
		if ($this->notfound === false) {
			$key_name = mb_strtolower($this->device_full_info['device_section']['name_singular'] . ', ' .
				$this->device_full_info['device_company']['title'] . ', ' .
				$this->device_full_info['device_info']['device']);
			return $key_name .
				', отзывы, оценка, мнения, написать отзыв, технические характеристики, фото, видео, обзор';
		} else {
			return 'Страница не найдена - ошибка 404';
		}
	}
	
	protected function getCanonical()
	{
		if ($this->notfound === false) {
			return '/'.$this->data['type'].'/'.$this->data['company'].'/'.$this->data['name'];
		} else {
			return '/notfound';
		}
	}
	
	protected function getMiddle()
	{
		if ($this->notfound === false) {
			$sr = [];

			$sr['for_admin_edit'] = '';
			if (isset($_SESSION['adm_auth']) && $_SESSION['adm_auth'] === true) {
				$srAdmEdit = [];
				$srAdmEdit['class'] = 'admin-link';
				$srAdmEdit['href'] = '/' . $this->config->adm_directory .
					'?view=device&id=' .
					$this->device_full_info['device_info']['id'];
				$srAdmEdit['anchor'] = 'Edit device';

				$sr['for_admin_edit'] = $this->getReplaceTemplate($srAdmEdit, 'tag_a');
			}

			$company = $this->device_full_info['device_company']['title'];
			$section = $this->device_full_info['device_section'];
			
			// цепочка навигации
			$sr['bread_crumb_sec_link'] = $this->device_full_info['device_section']['for_link'];
			$sr['bread_crumb_sec_name'] = $this->device_full_info['device_section']['name_plural'];
			$sr['bread_crumb_com_link'] = str_replace(
				' ',
				'_',
				mb_strtolower($this->device_full_info['device_company']['title'])
			);
			$sr['bread_crumb_com_name'] = $this->device_full_info['device_company']['title'];
			$sr['bread_crumb_dev_name'] = $this->device_full_info['device_info']['device'];
			
			$sr['name'] = $section['name_singular'].' '.$company.' '.$this->device_full_info['device_info']['device'];
			$sr['video'] = $this->getVideo();
			$sr['image'] = $this->config->address . '/' .
				$this->config->dir_img .
				$this->data['type'] . '_' .
				$this->data['company'] . '_' .
				$this->data['name'] . '.jpg';
			$sr['device_characteristics'] = $this->getCharacteristics();
			
			$sr['photo'] = $this->getPhoto();

			if ($this->device_full_info['device_company']['site']) {
				$sr_site['official_site'] = $this->device_full_info['device_company']['site'];
				$sr['official_site'] = $this->getReplaceTemplate($sr_site, 'device_site');
			} else {
				$sr['official_site'] = '';
			}
			
			// вывод оценок
			$sr['rating'] = $this->getRating();
			
			// вывод всех имеющихся отзывов
			$sr['comments_show'] = $this->getComments();
			
			$sr['message'] = '';
			$message = $this->getMessage();
			if ($message) {
				$sr_message['message'] = $message;
				$sr['message'] = $this->getReplaceTemplate($sr_message, 'message');
			}
			
			// добавление отзыва
			if ($this->session_for_current_device) {
				$sr['name_for_comment'] = $_SESSION['name_for_comment'];
				$sr['comment_for_comment'] = $_SESSION['comment_for_comment'];
			} else {
				$sr['name_for_comment'] = '';
				$sr['comment_for_comment'] = '';
			}
			$sr['device_id'] = $this->device_full_info['device_info']['id'];
			$sr['questions'] = $this->getQuestions();
			$sr['count_questions'] = count($this->question_list);
			
			// партнерки
			// Aliexpress
			$ali = '';
//			if ($this->device_full_info['device_info']['al_aliexpress']) {
//				$sr_affiliate_link['al_aliexpress'] = $this->device_full_info['device_info']['al_aliexpress'];
//				$ali = $this->getReplaceTemplate($sr_affiliate_link, 'affiliate_link_ali');
//			}

			// OZON
			$ozon= '';
//			if ($this->device_full_info['device_info']['al_ozon']) {
//				$sr_affiliate_link['al_ozon'] = $this->device_full_info['device_info']['al_ozon'];
//				$ozon = $this->getReplaceTemplate($sr_affiliate_link, 'affiliate_link_ozon');
//			}
			if (!$ozon && !$ali) {
				$sr['affiliate_links'] = 'Скоро будет известно.';
			} else {
				$sr['affiliate_links'] = $ozon . $ali;
			}

			$sr['device_ad'] = $this->getTemplate('device_ad');

			// mixupload.com, если секция "Наушники"
//			if ($this->device_full_info['device_section']['id'] == 4) {
//				$sr['device_ad'] = $this->getTemplate('device_mixupload');
//			}

			return $this->getReplaceTemplate($sr, 'device');
		} else {
			return $this->getTemplate('not_found');
		}
	}
	
	private function getPhoto()
	{
		$type = $this->device_full_info['device_section']['for_link'];
		$brand = str_replace(' ', '_', mb_strtolower($this->device_full_info['device_company']['title']));
		$device = str_replace(' ', '_', mb_strtolower($this->device_full_info['device_info']['device']));
		$path = '/' . $this->config->dir_img . $type . '/' . $brand . '/' . $device . '/';
		
		$alt = $this->device_full_info['device_section']['name_singular'] . ' ' .
			$this->device_full_info['device_company']['title'] . ' ' .
			$this->device_full_info['device_info']['device'];
		
		$photo_dir = $_SERVER['DOCUMENT_ROOT'] . $path;
		
		$files = scandir($photo_dir);
		$photo_count = count($files) - 2;
		if ($photo_count > 0) {
			$text = '';
			for ($i = 1; $i <= $photo_count; $i++) {
				$sr['src'] = $path . $i . '.jpg';
				$sr['alt'] = $alt . ' - фото ' .  $i;
				$sr['schema_ORG'] = ($i == 1) ?
					' itemprop=\'image\'' :
					'';
				$text .= $this->getReplaceTemplate($sr, 'device_photo_item');
			}
			return $text;
		} else {
			return '';
		}
	}
	
	private function getRating()
	{
		// считаю общую оценку устройства
		$countingRating = $this->device_full_info['counting_raiting'];
		if (!$countingRating) {
			return 'Оценок пока нет';
		}
		
		$ratingText = '';
		$avg_rating = 0;
		for ($i = 0; $i < count($this->question_list); $i++) {
			$sr['question_resume'] = $this->question_list[$i]['question'];
			$sr['rating'] = (isset($countingRating[ $this->question_list[$i]['id'] ])) ?
				round($countingRating[ $this->question_list[$i]['id'] ], 1) :
				0;
			$sr['bar_CSS'] = $sr['rating'] * 50;

			$ratingText .= $this->getReplaceTemplate($sr, 'device_rating_item');
			
			$avg_rating += $sr['rating'];
		}
		
		$global_sr['rating'] = $ratingText;
		$global_sr['rating_value'] = round(($avg_rating / count($this->question_list)), 1);
		$global_sr['rating_count'] = count($this->device_full_info['all_mod_comments']);
		if (($global_sr['rating_count'] % 10 == 1) &&
			($global_sr['rating_count'] != 11)
		) {
			$global_sr['ending'] = 'а';
		} else {
			$global_sr['ending'] = 'ов';
		}
		
		return $this->getReplaceTemplate($global_sr, 'device_rating');
	}
	
	private function getVideo()
	{
		$video = $this->video->getVideo($this->device_full_info['device_info']['id']);
		if ($video) {
			$video_block = '';
			for ($i = 0; $i < count($video); $i++) {
				$sr['source'] = $video[$i]['source'];
				$video_block .= $this->getReplaceTemplate($sr, 'device_video_item');
			}
		} else {
			$video_block = $this->getTemplate('device_video_not_have');
		}
		return $video_block;
	}
	
	private function getQuestions()
	{
		$title = array('отлично', 'хорошо', 'средне', 'плохо', 'ужасно');
		$quest = '';
		for ($i = 0; $i < count($this->question_list); $i++) {
			$sr['question_id'] = $this->question_list[$i]['id'];
			$sr['question'] = $this->question_list[$i]['question'];
			$sr['number'] = $i;
			
			// проверяю, отвечал ли пользователь на текущий вопрос
			$variant = ((isset($_SESSION['rating_'.$i])) && ($this->session_for_current_device))
				? $_SESSION['rating_'.$i]
				: false;
			
			$rating_items = '';
			for ($j = 0; $j < 5; $j++) {
				$deep_sr['check'] = ($variant == 5 - $j)
					? 'checked'
					: '';
				$deep_sr['item_num'] = 5 - $j;
				$deep_sr['number'] = $i;
				$deep_sr['title'] = $title[$j];
				$rating_items .= $this->getReplaceTemplate($deep_sr, 'rating_item');
			}
			$sr['rating_items'] = $rating_items;
			
			$quest .= $this->getReplaceTemplate($sr, 'rating');
		}
		return $quest;
	}
	
	private function getCharacteristics()
	{
		$ch = $this->characteristic->getAllOnField('device_id', $this->device_full_info['device_info']['id']);
		$text = '';
		for ($i = 0; $i < count($ch); $i++) {
			$sr['name']  = $ch[$i]['name'];
			$sr['value'] = $ch[$i]['value'];
			$text .= $this->getReplaceTemplate($sr, 'device_characteristics');
		}
		return $text;
	}
	
	private function getComments()
	{
		$all_mod_comments = $this->device_full_info['all_mod_comments'];
		if (!$all_mod_comments) {
			return 'Оставь мнение первым';
		}
		$text = '';
		for ($i = 0; $i < count($all_mod_comments); $i++) {
			$sr['for_admin_edit'] = '';
			if (isset($_SESSION['adm_auth']) && $_SESSION['adm_auth'] === true) {
				$srAdmEdit = [];
				$srAdmEdit['class'] = 'admin-link';
				$srAdmEdit['href'] = '/' . $this->config->adm_directory .
					'?view=comments&id=' .
					$all_mod_comments[$i]['id'];
				$srAdmEdit['anchor'] = 'Edit comment';

				$sr['for_admin_edit'] = $this->getReplaceTemplate($srAdmEdit, 'tag_a');
			}
			$sr['name'] = $all_mod_comments[$i]['name'];
			$sr['date'] = $this->formatDate($all_mod_comments[$i]['date']);
			$sr['datetime'] = date('c', $all_mod_comments[$i]['date']);
			$sr['schema_ORG_datePublished'] = date('Y-m-d', $all_mod_comments[$i]['date']);
			
			// my BB code -> HTML
			$comment = $all_mod_comments[$i]['comment'];
			$pattern = '~\[url=(.+?)\](.+?)\[/url\]~';
			$replacement = '<a href=\'$1\'>$2</a>';
			$sr['comment'] = preg_replace($pattern, $replacement, $comment);
			
			$text .= $this->getReplaceTemplate($sr, 'comment_snippet');
		}
		return $text;
	}
}
