<?php
	require_once 'config_class.php';
	require_once 'checkvalid_class.php';
	require_once 'database_class.php';
	
	abstract class GlobalClass {
		
		private $db;
		private $table_name;
		protected $config;
		protected $valid;
		
		// $db - это объект класса DataBase
		protected function __construct($table_name, $db) {
			$this->db = $db;
			$this->table_name = $table_name;
			$this->config = new Config();
			$this->valid = new CheckValid();
		}
		
		public function insertAndGetId($new_values) {
			return $this->db->insertAndGetId($this->table_name, $new_values);
		}
		
		public function insetFewStrings($arr_fields, $arr_new_values) {
			return $this->db->insetFewStrings($this->table_name, $arr_fields, $arr_new_values);
		}
		
		// $new_values - ассоциативный массив, где "поле" -> "значение"
		protected function add($new_values) {
			return $this->db->insert($this->table_name, $new_values);
		}
		
		protected function updateOnID($id, $upd_fields) {
			return $this->db->updateOnID($this->table_name, $upd_fields, $id);
		}
		
		public function editString($upd_fields, $field_in, $value_in) {
			return $this->db->editString($this->table_name, $upd_fields, $field_in, $value_in);
		}
		
		public function deleteOnID($id) {
			return $this->db->deleteOnID($this->table_name, $id);
		}
		
		public function delete($where) {
			return $this->db->delete($this->table_name, $where);
		}
		
		public function deleteAll() {
			return $this->db->deleteAll($this->table_name);
		}
		
		protected function getField($field_out, $field_in, $value_in) {
			return $this->db->getField($this->table_name, $field_out, $field_in, $value_in);
		}
		
		public function getFieldOnID($id, $field) {
			return $this->db->getFieldOnID($this->table_name, $id, $field);
		}
		
		public function setField($field, $value, $field_in, $value_in) {
			return $this->db->setField($this->table_name, $field, $value, $field_in, $value_in);
		}
		
		protected function setFieldOnID($id, $field, $value) {
			return $this->db->setFieldOnID($this->table_name, $id, $field, $value);
		}
		
		public function get($id) {
			return $this->db->getElementOnID($this->table_name, $id);
		}
		
		public function getElementOnField($field_name, $field_value) {
			return $this->db->getElementOnField($this->table_name, $field_name, $field_value);
		}
		
		public function getAll($order = '', $up = true) {
			return $this->db->getAll($this->table_name, $order, $up);
		}
		
		public function getAllOnField($field, $value, $order = '', $up = true) {
			return $this->db->getAllOnField($this->table_name, $field, $value, $order, $up);
		}
		
		public function getRandomElement($count) {
			return $this->db->getRandomElements($this->table_name, $count);
		}
		
		public function getLastId() {
			return $this->db->getLastID($this->table_name);
		}
		
		public function getCount($where = '') {
			return $this->db->getCount($this->table_name, $where);
		}
		
		// $fields - одномерный массив
		public function getAllWithTheFields($fields) {
			return $this->db->getAllWithTheFields($this->table_name, $fields);
		}
		
		// $fields - одномерный массив
		public function getAllWithTheFieldsAndWhere($fields, $where) {
			return $this->db->getAllWithTheFieldsAndWhere($this->table_name, $fields, $where);
		}
		
		public function getDistinctWithTheFieldsAndWhere($fields, $where) {
			return $this->db->getDistinctWithTheFieldsAndWhere($this->table_name, $fields, $where);
		}
		
		protected function isExists($field, $value) {
			return $this->db->isExists($this->table_name, $field, $value);
		}
		
		protected function search($words, $fields) {
			return $this->db->search($this->table_name, $words, $fields);
		}
		
		public function getAllWithWhere($where) {
			return $this->db->getAllWithWhere($this->table_name, $where);
		}
		
		public function getAllWithWhereAndOrder($where, $order, $up) {
			return $this->db->getAllWithWhereAndOrder($this->table_name, $where, $order, $up);
		}
		
		// сортировка по Id по убыванию
		public function getFewFreshString($where, $limit) {
			return $this->db->getFewFreshString($this->table_name, $where, $limit);
		}
		
		public function getFieldsWithOrder($fields, $order, $up)
		{
			return $this->db->getFieldsWithOrder($this->table_name, $fields, $order, $up);
		}
		
		public function getFields($fields)
		{
			return $this->db->getFields($this->table_name, $fields);
		}
		
	}
?>