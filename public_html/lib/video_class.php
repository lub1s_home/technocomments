<?php

require_once 'global_class.php';

class Video extends GlobalClass {

	public function __construct($db) {
		parent::__construct('video', $db);
	}
	
	public function getVideo($device_id) {
		return $this->getAllWithTheFieldsAndWhere(array('source'), '`device_id` = '.$device_id);
	}
	
	public function getVideoForDevice($device_id) {
		return $this->getAllOnField('device_id', $device_id);
	}
	
	public function addVideo($device_id, $source) {
		return $this->add(array('device_id' => $device_id, 'source' => $source));
	}
	
	public function editVideo($id_video, $new_source) {
		return $this->updateOnID($id_video, array('source' => $new_source));
	}
	
}

?>