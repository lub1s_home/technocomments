<?php
require_once 'adm_modules_class.php';
require_once 'company_class.php';

class Adm_CompanyContent extends Adm_Modules 
{
	private $company_obj;
	
	public function __construct($db) 
	{
		parent::__construct($db);
		
		$this->company_obj = new Company($db);
	} 
	
	protected function getTitle() 
	{
		return 'Бренды';
	}
	
	protected function getMiddle() 
	{
		$company = $this->company_obj->getCompanyArrSortTitle();
		$text = '';
		for ($i = 0; $i < count($company); $i++) 
		{
			$sr['id'] = $company[$i]['id'];
			$sr['title'] = $company[$i]['title'];
			$sr['site'] = $company[$i]['site'];
			$sr['wordstat'] = $company[$i]['wordstat'];
			
			$text .= $this->getReplaceTemplate($sr, 'adm_company_snippet');
		}
		$new_sr['string'] = $text;
		return $this->getReplaceTemplate($new_sr, 'adm_company');
	}	
}
?>