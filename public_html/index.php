<?php
mb_internal_encoding('UTF-8');

require_once 'lib/database_class.php';
require_once 'lib/devicecontent_class.php';
require_once 'lib/frontpagecontent_class.php';
require_once 'lib/messagecontent_class.php';
require_once 'lib/offercontent_class.php';
require_once 'lib/regcontent_class.php';
require_once 'lib/sectioncontent_class.php';
require_once 'lib/xmlsitemap_content.php';
require_once 'lib/notfoundcontent_class.php';
	
$db = DataBase::getObject();
$view = (isset($_GET['view'])) ? 
	$_GET['view'] : 
	'';

switch ($view)
{
	case '':
		$content = new SectionContent($db);
		break;
	case 'device':
		$content = new DeviceContent($db);
		break;
	case 'message':
		$content = new MessageContent($db);
		break;
	case 'reg':
		$content = new RegContent($db);
		break;
	case 'section':
		$content = new SectionContent($db);
		break;
	case 'sitemap':
		$content = new XMLSiteMapContent($db);
		break;
	case 'offer':
		$content = new OfferContent($db);
		break;
	case 'notfound':
		$content = new NotFoundContent($db);
		break;

	default: $content = new NotFoundContent($db);
}
echo $content->getContent();
?>